{
  description = "cvoges12-mobile-x220-nixos-init";
    
  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs?ref=nixos-23.11";
    nixpkgs-unstable.url = "github:NixOS/nixpkgs?ref=nixos-unstable";
    disko = {
      url = "github:nix-community/disko";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    nixos-hardware.url = "github:NixOS/nixos-hardware/master";
    home-manager = {
      url = "github:nix-community/home-manager?ref=release-23.11";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    nur.url = "github:nix-community/NUR";
    flake-programs-sqlite = {
      url = "github:wamserma/flake-programs-sqlite";
      inputs.nixpkgs.follows = "nixpkgs";
    };
  };

  outputs = inputs@{
    self,
    nixpkgs,
    nixpkgs-unstable,
    nixos-hardware,
    home-manager,
    disko,
    nur,
    flake-programs-sqlite,
    ...
  }: let
    systems = [
      { hostname = "cvoges12-mobile-x220-nixos"; }
      { hostname = "nixos-minimal"; }
    ];
    
    getField = key: set:
      set."${key}";

    getSet = key: value: sets:
      builtins.head
        (builtins.filter (set: getField key set == value) sets);

    genConfigSet = let
      hostnames = builtins.foldl' (xs: x: xs ++ [ x.hostname ])  [] systems;
    in f: nixpkgs.lib.genAttrs
      hostnames
      (hostname: f (getSet "hostname" hostname systems));
  in {
    formatter.x86_64-linux = nixpkgs.legacyPackages.x86_64-linux.nixpkgs-fmt;

    nixosConfigurations = genConfigSet (
      args@{
        hostname ? "null",
        system ? "x86_64-linux",
        pkgs ? nixpkgs.legacyPackages.${system},
        ...
      }: nixpkgs.lib.nixosSystem {
        inherit system;

        specialArgs = args // {
          inherit self inputs;
        };

        modules = [
          ./nixos/modules
          ./nixos/systems/${hostname}.nix

          ./modules/disko.nix
          disko.nixosModules.disko
        ];
      }
    );

    homeConfigurations = genConfigSet (
      args@{
        hostname ? "null",
        system ? "x86_64-linux",
        pkgs ? nixpkgs.legacyPackages.${system},
        ...
      }: home-manager.lib.homeManagerConfiguration {
        inherit pkgs;
        extraSpecialArgs = args // {
          inherit self inputs;
        };
        
        modules = [
          ./home-manager/modules
          ./home-manager/systems/${hostname}.nix
        ];
      }
    );
    
    diskoConfigurations = let
      diskoModules = config: {
        disko.devices = (nixpkgs.lib.evalModules {
          modules = [
            { options.disko.devices = nixpkgs.lib.mkOption {}; }
            ./modules/disko.nix
            ./disko/${config}.nix
          ];
        }).config.disko.devices;
      };
    in (diskoModules "thumbdrive");
  };
}
