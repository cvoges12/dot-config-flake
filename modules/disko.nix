{ config, lib, disko, ... }:

with lib;

let
  cfg = config.localModules.disko;
in {
  options.localModules.disko = {
    enable = mkEnableOption (mdDoc "disko");
    
    nvme = {
      enable = mkEnableOption (mdDoc "NVMe");

      disks = mkOption {
        type = with types; listOf str;
        default = [];
      };

      boot.enable = mkEnableOption (mdDoc "bootable");
      encrypt.enable = mkEnableOption (mdDoc "encryption");
      lvm.enable = mkEnableOption (mdDoc "lvm");

      mountPoint = mkOption {
        type = types.str;
        default = "/";
      };

      raid = {
        enable = mkEnableOption (mdDoc "raid");
        level = mkOption {
          type = types.str;
          default = "raid5";
        };
      };

      swap = {
        enable = mkEnableOption (mdDoc "swap");
        size = mkOption {
          type = types.str;
          default = "4G";
          example = "32G";
        };
      };
    };

    ssd = {
      enable = mkEnableOption (mdDoc "SSD");

      disks = mkOption {
        type = with types; listOf str;
        default = [];
      };

      boot.enable = mkEnableOption (mdDoc "bootable");
      encrypt.enable = mkEnableOption (mdDoc "encryption");
      lvm.enable = mkEnableOption (mdDoc "lvm");

      mountPoint = mkOption {
        type = types.str;
        default = "/";
      };

      raid = {
        enable = mkEnableOption (mdDoc "raid");
        level = mkOption {
          type = types.str;
          default = "raid5";
        };
      };

      swap = {
        enable = mkEnableOption (mdDoc "swap");
        size = mkOption {
          type = types.str;
          default = "4G";
          example = "32G";
        };
      };
    };

    hdd = {
      enable = mkEnableOption (mdDoc "HDD");

      disks = mkOption {
        type = with types; listOf str;
        default = [];
      };

      boot.enable = mkEnableOption (mdDoc "bootable");
      encrypt.enable = mkEnableOption (mdDoc "encryption");
      lvm.enable = mkEnableOption (mdDoc "lvm");

      mountPoint = mkOption {
        type = types.str;
        default = "/";
      };

      raid = {
        enable = mkEnableOption (mdDoc "raid");
        level = mkOption {
          type = types.str;
          default = "raid5";
        };
      };

      swap = {
        enable = mkEnableOption (mdDoc "swap");
        size = mkOption {
          type = types.str;
          default = "4G";
          example = "32G";
        };
      };
    };
  };

  config = mkIf cfg.enable {
    disko.devices = {
      disk = lib.lists.foldl (x: y: lib.recursiveUpdate x y) {} (lib.lists.map (diskType:
        (lib.attrsets.genAttrs (lib.lists.filter (x: builtins.pathExists x) cfg."${diskType}".disks) (device: {
          inherit device;
          name = lib.replaceStrings [ "/" ] [ "_" ] device;
          type = "disk";

          content = {
            type = "gpt";
            partitions = lib.mkMerge [
              (lib.mkIf cfg."${diskType}".boot.enable {
                # GRUB MBR for BIOS compat
                BOOT = {
                  size = "1M";
                  type = "EF02";
                  device = "${device}-part1";
                };
                
                ESP = {
                  size = "512M";
                  type = "EF00";
                  device = "${device}-part2";

                  content =
                    if cfg."${diskType}".raid.enable then
                      {
                        type = "mdadm";
                        name = "boot";
                      }
                    else
                      {
                        type = "filesystem";
                        format = "vfat";
                        mountpoint = "/boot";
                        mountOptions = [ "defaults" ];
                      };
                };
              })

              {
                primary = (
                  # !(swap -> lvm)
                  if (!((!cfg."${diskType}".swap.enable) || cfg."${diskType}".lvm.enable)) then
                    { end = "-${cfg."${diskType}".swap.size}"; }
                  else
                    { size = "100%"; }
                ) // {
                  content = let
                    contents =
                      if cfg."${diskType}".lvm.enable then
                        {
                          type = "lvm_pv";
                          vg = "${diskType}_vg";
                        }
                      else
                        {
                          type = "filesystem";
                          format = "vfat";
                          mountpoint = cfg."${diskType}".mountPoint;
                          mountOptions = [ "defaults" ];
                        };
                  in {
                    device =
                      if cfg."${diskType}".boot.enable then
                        "${device}-part3"
                      else
                        "${device}-part1";

                  } // (
                    if cfg."${diskType}".encrypt.enable then
                      {
                        type = "luks";
                        name = "crypted";
                        passwordFile = "/tmp/secret.key";
                        content = contents;
                      }
                    else
                      contents
                  );
                };
              }

              (lib.mkIf (cfg."${diskType}".swap.enable && (!cfg."${diskType}".lvm.enable)) {
                swap = {
                  size = "100%";
                  content = {
                    device =
                      if cfg."${diskType}".boot.enable then
                        "${device}-part4"
                      else
                        "${device}-part2";

                    type = "swap";
                    randomEncryption = lib.mkIf (!cfg."${diskType}".encrypt.enable) true;
                    resumeDevice = true;
                  };
                };
              })
            ];
          };
        }))
      ) (lib.lists.foldl (x: y: x ++ y) [] [
        (if cfg.nvme.enable then  [ "nvme" ] else [])
        (if cfg.ssd.enable then  [ "ssd" ] else [])
        (if cfg.hdd.enable then  [ "hdd" ] else [])
      ]));
      
      mdadm = mkIf (
        (cfg.nvme.raid.enable && cfg.nvme.boot.enable) ||
        (cfg.ssd.raid.enable && cfg.ssd.boot.enable) ||
        (cfg.hdd.raid.enable && cfg.hdd.boot.enable)
      ) {
        boot = {
          type = "mdadm";
          level = 1;
          metadata = "1.0";
          content = {
            type = "filesystem";
            format = "vfat";
            mountpoint = "/boot";
            mountOptions = [ "defaults" ];
          };
        };
      };
      
      lvm_vg = lib.lists.foldl (x: y: recursiveUpdate x y) {} (lib.lists.map (vg: {
        "${vg}_vg" = {
          type = "lvm_vg";
          lvs = {
            root = {
              size = "100%FREE";
              lvm_type = mkIf cfg."${vg}".raid.enable cfg."${vg}".raid.level;
              content = {
                type = "filesystem";
                format = "ext4";
                mountpoint = cfg."${vg}".mountPoint;
                mountOptions = [ "defaults" ];
              };
            };
            
            swap = lib.mkIf cfg."${vg}".swap.enable {
              size = cfg."${vg}".swap.size;
              content = {
                type = "swap";
                randomEncryption = false; #lib.mkIf (!cfg.crypt.enable) true;
                resumeDevice = true;
              };
            };
          };
        };
      }) ["nvme" "ssd" "hdd"]);
    };
  };
}
