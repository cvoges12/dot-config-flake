{ pkgs, ... }:

{
  localModules = {
    disko = {
      enable = true;

      hdd = {
        enable = true;

        disks = [ ];

        boot.enable = true;
        lvm.enable = true;
        raid.enable = true;

        swap = {
          enable = true;
          size = "32G";
        };
      };
    };

    audio.enable = true;
    command-not-found.enable = true;
    docs.enable = true;
    fonts.enable = true;
    hmInstall.enable = true;
    hw.thinkpad-x220.enable = true;
    locale.zone = "America/Chicago";
    power.enable = true;
    print.enable = true;
    shell.fish.enable = true;
    ssh.enable = true;
    wm = {
      #sway.enable = true;
      i3.enable = true;
    };
    xdg.enable = true;

    keymap = {
      enable = true;
      variant = "colemak";
    };

    network = {
      enable = true;
      hostname = "cvoges12-home-desktop-nixos";
    };

    nix = {
      enable = true;
      gc = true;
    };

    security = {
      enable = true;
      finger-print = true;
      swaylock = true;
      yubikey = true;
    };

    root = {
      enable = true;
      hashedPassword = "$6$7repDVxodMWzfRKI$VtVIUkuahz7mGxKulfcJOwb1NbCsKGkjq6HWTpRhZeD0Qf7/vPWKLIeGyBcjJ/494wbh5skpr6PeAGgh0kdUx/";
      keys = [ "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQDzeEFlofivGruumUCnYyo9cNcBkG5jKG1KZMNWGeYUl3guYFzykVJrZA/MfgREiC/zEp12CUpzowLVZ/7NkokYU2+7cCbb0RwPcI09DjOFgYm4lOWHpvxL4pe4/ufB3xskRJBH7e/jZtglx+11UgXAeieF7uHY07p5a2aEO4JwwXiEN8EzOHWgKKoKSWVyzXEGB/uEnfr34mbkWh1/mqBr1NNh2nBw3RexQ+4inBlpZ+G6yabf3bd3HfnDmLwDVkLanY2IWPF9wCAnPj+JVI0ztEaCBVpPh1bFhkW0/u2Rs68gbZ599WyMeUieJOmCbvqS7WQM/WGMtqEMAvoazlHKJKbXImRjalx1marFNabwXbuwT0mYXPmG8glIKKaEyUUtuTidqjXQL+/gFKW0mUk3JShlkZ/0IJ6VnyQSyeAHMovYCz/hHYXYLut0NULsX6A1ehuTX5rvbx2amZu61Cl48zMrAv4locsY3A6bjPk1fYRUrsfELEItjssoJfWCupM=" ];
    };

    user = {
      enable = true;
      name = "cvoges12";
      hashedPassword = "$6$s0Wt17sEULuy26Fy$3ELdP62mK4b7aNCikT9e3lrXQx5MYMqFI2.vEF/Guj570LQmO71WDQ91R7/5x0YJJWI6cH7z.F0Jne78uiJeI1";
      keys = [ "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQDzeEFlofivGruumUCnYyo9cNcBkG5jKG1KZMNWGeYUl3guYFzykVJrZA/MfgREiC/zEp12CUpzowLVZ/7NkokYU2+7cCbb0RwPcI09DjOFgYm4lOWHpvxL4pe4/ufB3xskRJBH7e/jZtglx+11UgXAeieF7uHY07p5a2aEO4JwwXiEN8EzOHWgKKoKSWVyzXEGB/uEnfr34mbkWh1/mqBr1NNh2nBw3RexQ+4inBlpZ+G6yabf3bd3HfnDmLwDVkLanY2IWPF9wCAnPj+JVI0ztEaCBVpPh1bFhkW0/u2Rs68gbZ599WyMeUieJOmCbvqS7WQM/WGMtqEMAvoazlHKJKbXImRjalx1marFNabwXbuwT0mYXPmG8glIKKaEyUUtuTidqjXQL+/gFKW0mUk3JShlkZ/0IJ6VnyQSyeAHMovYCz/hHYXYLut0NULsX6A1ehuTX5rvbx2amZu61Cl48zMrAv4locsY3A6bjPk1fYRUrsfELEItjssoJfWCupM=" ];
    };

    packages = with pkgs; [
      eza
      git
      wget
      curl
      ripgrep-all
      qpwgraph
      zenith # use `zenith-nvidia` for nvidia gpu
      intel-gpu-tools # for `intel_gpu_top`
      # `nvtop` instead of `intel_gpu_top` once kernel >v5.19 and intel drivers
      whois
      netcat
      nmap
      traceroute
      rsync
      testdisk
      zip
      unzip
      unrar
      xz
      acpi
      hwinfo
      lshw
    ];
  };

  #security.sudo.enable = false;

  services.udev.packages = with pkgs; [
    android-udev-rules
  ];
  
  programs = {
    light.enable = true;
    kdeconnect.enable = true;
    dconf.enable = true; # required for gtk
  };

  system.stateVersion = "23.05";
}
