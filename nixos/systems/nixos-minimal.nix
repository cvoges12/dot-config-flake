_:

{
  localModules = {
    network = {
      enable = true;
      hostname = "nixos-minimal";
    };
    ssh.enable = true;
  };
}
