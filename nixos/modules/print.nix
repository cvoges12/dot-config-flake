{ config, lib, ... }:

with lib;

let
  cfg = config.localModules.print;
in {
  options.localModules.print = {
    enable = mkEnableOption (mdDoc "print");
  };

  config = mkIf cfg.enable {
    services.printing.enable = true;
  };
}
