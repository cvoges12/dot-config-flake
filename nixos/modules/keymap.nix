{ config, lib, ... }:

with lib;

let
  cfg = config.localModules.keymap;
in {
  options.localModules.keymap = {
    enable = mkEnableOption (mdDoc "key map");
    variant = mkOption {
      type = types.str;
      default = "qwerty";
      example = "colemak";
    };
  };

  config = mkIf cfg.enable {
    services.xserver = mkIf (cfg.variant == "colemak") {
      layout = "us";
      xkbVariant = "colemak";
    };

    console.useXkbConfig = true;
  };
}
