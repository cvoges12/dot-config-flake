{ config, lib, ... }:

with lib;

let
  cfg = config.localModules.locale;
in {
  options.localModules.locale = {
    zone = mkOption {
      type = types.str;
      default = "Etc/UTC";
      example = "America/Chicago";
    };
  };

  config = {
    time.timeZone = cfg.zone;

    #i18n = {
    #  defaultLocale = "en_US.UTF-8";

    #  supportedLocales = [
    #    "en_US.UTF-8/UTF-8"
    #    "de_DE.UTF-8/UTF-8"
    #    "nl_NL.UTF-8/UTF-8"
    #    "zh_CN.UTF-8/UTF-8"
    #    "ja_JP.UTF-8/UTF-8"
    #    "ko_KR.UTF-8/UTF-8"
    #  ];

    #  extraLocaleSettings = {
    #    LC_ADDRESS =        "en_US.UTF-8";
    #    LC_IDENTIFICATION = "en_US.UTF-8";
    #    LC_MEASUREMENT =    "de_DE.UTF-8";
    #    LC_MONETARY =       "en_US.UTF-8";
    #    LC_NAME =           "en_US.UTF-8";
    #    LC_NUMERIC =        "en_US.UTF-8";
    #    LC_PAPER =          "en_US.UTF-8";
    #    LC_TELEPHONE =      "en_US.UTF-8";
    #    LC_TIME =           "en_DK.UTF-8";
    #  };
    #};
  };
}
