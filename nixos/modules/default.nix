{ ... }:

{
  imports = [
    ./audio.nix
    ./command-not-found.nix
    ./docs.nix
    ./fonts.nix
    ./hmInstall.nix
    ./hw
    ./keymap.nix
    ./locale.nix
    ./neovim.nix
    ./network.nix
    ./nix.nix
    ./packages.nix
    ./power.nix
    ./print.nix
    ./root.nix
    ./security.nix
    ./shell
    ./ssh.nix
    ./tmpwatch.nix
    ./user.nix
    ./wm
    ./xdg.nix
  ];
}
