{ config, lib, ... }:

with lib;

let
  cfg = config.localModules.power;
in {
  options.localModules.power = {
    enable = lib.mkEnableOption (lib.mdDoc "opinionated font choices");
  };

  config = lib.mkIf cfg.enable {
    services = {
      upower.enable = true;
    };

    # uncomment when it can collect information
    #powerManagement.powertop.enable = true;
  };
}
