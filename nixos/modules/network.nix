{ config, lib, ... }:

with lib;

let
  cfg = config.localModules.network;
in {
  options.localModules.network = {
    enable = mkEnableOption (mdDoc "networking");

    hostname = mkOption {
      type = types.str;
      default = "";
      example = "foobar";
      description = "Hostname";
    };

    dns = mkOption {
      type = types.listOf types.str;
      default = [
        "9.9.9.9"
        "149.112.112.112"
        "2620:fe::fe"
        "2620:fe::9"
      ];
      example = [ "1.1.1.1" ];
      description = "List of domain name servers";
    };

    log = mkOption {
      type = types.str;
      default = "DEBUG";
    };
  };

  config = mkIf cfg.enable {
    networking = {
      hostName = cfg.hostname;
      networkmanager = {
        enable = true;
        logLevel = cfg.log;
        appendNameservers = cfg.dns;
      };
      useDHCP = mkDefault true;
    };

    users.users.${config.localModules.user.name}.extraGroups = [
      "networkmanager"
    ];
  };
}
