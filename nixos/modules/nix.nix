{ config, lib, nixpkgs-unstable, ... }:

with lib;

let
  cfg = config.localModules.nix;
in {
  options.localModules.nix = {
    enable = mkEnableOption (mdDoc "nix");
    gc = mkEnableOption (mdDoc "gc");
  };

  config = mkIf cfg.enable {
    
    nixpkgs = {
      overlays = [
        (final: prev: {
          unstable = inputs.nixpkgs-unstable.legacyPackages.${prev.system};
        })
      ];
      config = {
        allowUnfree = true;
        packageOverrides = pkgs: {
          nur = import (builtins.fetchTarball "https://github.com/nix-community/NUR/archive/master.tar.gz") {
            inherit pkgs;
          };
        };
      };
    };

    nix = {
      gc = mkIf cfg.gc {
        automatic = true;
        dates = "8:30";
      };
      
      settings = {
        experimental-features = [
          "nix-command"
          "flakes"
          "repl-flake"
        ];
        
        auto-optimise-store = true;
      };
    };
  };
}
