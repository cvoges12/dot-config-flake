{ config, pkgs, lib, ... }:

with lib;

let
  cfg = config.localModules.fonts;
in {
  options.localModules.fonts = {
    enable = mkEnableOption (mdDoc "opinionated font choices");
  };

  config = mkIf cfg.enable {
    fonts = {
      enableDefaultPackages = true;

      packages = with pkgs; [

        # https://github.com/ryanoasis/nerd-fonts#patched-fonts
        (nerdfonts.override { fonts = [

          # no #
          #"3270" # pixelated / low res
          #"Gohu" # pixelated / low res
          #"OpenDyslexic" # can't read
          #"ProFont" # pixelated / low res
          #"Terminus" # pixelated / low res

          # idk #
          #"Arimo" # no pics
          #"AurulentSansMono" # no link
          #"BigBlueTerminal" # no link
          #"BitstreamVeraSansMono" # no link
          #"CodeNewRoman" # no link
          #"DroidSansMono" # no link
          #"FiraMono" # no pics
          #"Go-Mono" # no pics
          #"HeavyData" # no link
          #"iA-Writer" # no pics
          #"InconsolataLGC" # no pics
          #"Monofur" # no link
          #"LiberationMono" # no pics
          #"Monoid" # no pics
          #"ProggyClean" # no link
          #"SourceCodePro" # no pics

          # maybe #
          #"Agave"
          #"AnonymousPro"
          #"CascadiaCode"
          #"Cousine"
          #"DaddyTimeMono"
          #"FantasqueSansMono"
          #"FiraCode"
          #"Hack"
          #"Hasklig"
          #"Inconsolata"
          #"InconsolataGo"
          #"Iosevka"
          #"IosevkaTerm"
          #"Lekton"
          #"Lilex"
          #"Mononoki"
          #"MPlus"
          #"Overpass"
          #"RobotoMono"
          #"ShareTechMono"
          #"SpaceMono"
          #"Tinos" # serifs
          #"Ubuntu"
          #"UbuntuMono"
          #"VictorMono"

          # comic sans
          "ComicShannsMono" # comic sans

          # terminal
          "JetBrainsMono"

          # de facto
          "Noto"
          "Ubuntu"
          "DejaVuSansMono"
        ]; })
      ];

      fontconfig.defaultFonts = {
        serif = [
          "NotoSerif Nerd Font Propo,NotoSerif NFP"
          "DejaVu Serif"
        ];
        sansSerif = [
          "NotoSans Nerd Font Propo,NotoSans NFP"
          "Ubuntu Nerd Font Propo"
          "DejaVu Sans"
        ];
        monospace = [
          "JetBrainsMono Nerd Font Mono,JetBrainsMono NFM"
          #"ComicShannsMono Nerd Font Mono"
        ];
      };
    };
  };
}
