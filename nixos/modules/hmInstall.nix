{ config, pkgs, lib, ... }:

with lib;

let
  cfg = config.localModules.hmInstall;
in {
  options.localModules.hmInstall = {
    enable = mkEnableOption (mdDoc "install home-manager");
  };

  config = mkIf cfg.enable {
    systemd.services.hmInstall = {
      script = ''
        if [ ! -f ~/.local/share/hmInstall ]; then
          rm -rf ~/.config/* ~/.local/* ~/.cache/*

          # creates home-manager profile directory
          nix --extra-experimental-features "nix-command flakes" run home-manager/master -- init --switch

          home-manager switch --flake "gitlab:cvoges12/dot-config-flake#$(hostname)"

          touch ~/.local/share/hmInstall
        fi
      '';

      wantedBy = [ "multi-user.target" ];
      wants = [ "network-online.target" ];
      after = [ "network-online.target" "network.target" ];
      requires = [ "network-online.target" ];
      
      path = [
        pkgs.nix
        pkgs.hostname
        pkgs.home-manager
      ];

      serviceConfig = {
        User = "cvoges12";
        Group = "users";
        Restart = "on-failure";
        RestartSec = 10;
      };
    };
  };
}
