{ config, lib, ... }:

with lib;

let
  cfg = config.localModules.packages;
in {
  options.localModules.packages = mkOption {
    type = with types; listOf package;
    default = [];
  };

  config = {
    environment = {
      defaultPackages = lib.mkForce [];
      systemPackages = cfg;
    };
  };
}
