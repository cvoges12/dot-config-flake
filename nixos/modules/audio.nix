{ config, pkgs, lib, ... }:

with lib;

let
  cfg = config.localModules.audio;
in {
  options.localModules.audio = {
    enable = mkEnableOption (mdDoc "batteries included audio stack");
  };

  config = mkIf cfg.enable {
    sound.enable = false;
    
    services.pipewire = {
      enable = true;
      jack.enable = true;
      alsa = {
        enable = true;
        support32Bit = true;
      };
      pulse.enable = true;
      audio.enable = true;
      wireplumber.enable = true;
    };

    systemd.user.services.pipewire-pulse.path = with pkgs; [ pulseaudio ];
    
    environment.systemPackages = with pkgs; [ pulseaudio ];

    users.users.${config.localModules.user.name}.extraGroups = [
      "audio"
    ];
  };
}
