{ config, pkgs, lib, ... }:

with lib;

let
  cfg = config.localModules.security;
in {
  options.localModules.security = {
    enable = mkEnableOption (mdDoc "security");
    swaylock = mkEnableOption (mdDoc "swaylock");

    # set up yubikey
    yubikey = mkEnableOption (mdDoc "yubikey");

    # set up finger prints
    finger-print = mkEnableOption (mdDoc "finger print sensor");
  };

  config = mkIf cfg.enable {
    security = {
      apparmor.enable = true;
      audit = {
        enable = true;
        rules = [
          "-a exit,always -F arch=b64 -S execve"
        ];
      };
      auditd.enable = true;
      polkit.enable = true;
      pam.services = {
        login.fprintAuth = cfg.finger-print;
        swaylock.text = mkIf cfg.swaylock "auth include login";
      };
    };

    services = {
      fprintd.enable = cfg.finger-print;

      udev.packages = with pkgs; lists.optional cfg.yubikey yubikey-personalization;
    };

    environment.systemPackages = [
      # once polkit-kde is no longer marked as broken
      #pkgs.libsForQt514.polkit-kde-agent
    ];

    # once polkit-kde is no longer marked as broken
    #systemd.user.services.polkit-kde-authentication-agent-1 = {
    #  description = "polkit-kde-authentication-agent-1";
    #  wantedBy = [ "graphical-session.target" ];
    #  wants = [ "graphical-session.target" ];
    #  after = [ "graphical-session.target" ];
    #  serviceConfig = {
    #    Type = "simple";
    #    ExecStart = "${pkgs.polkit-kde-agent}/libexec/polkit-kde-authentication-agent-1";
    #    Restart = "on-failure";
    #    RestartSec = 1;
    #    TimeoutStopSec = 10;
    #  };
    #};
  };
}
