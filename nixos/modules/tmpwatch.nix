{ config, pkgs, lib, ... }:

with lib;

let
  cfg = config.localModules.tmpwatch;
in {
  options.localModules.tmpwatch = {
    enable = lib.mkOption {
      type = types.bool;
      default = true;
      example = false;
      description = lib.mdDoc "automatically removes old files in /tmp";
    };
  };

  config = lib.mkIf cfg.enable {
    environment.systemPackages = with pkgs; [ tmpwatch ];

    services.cron = {
      enable = true;
      systemCronJobs = [
        "0 0 1 * *     root     tmpwatch -maf 240 /tmp"
      ];
    };
  };
}
