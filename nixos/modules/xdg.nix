{ config, pkgs, lib, ... }:

with lib;

let
  cfg = config.localModules.xdg;
in {
  options.localModules.xdg = {
    enable = mkEnableOption (mdDoc "xdg");
  };

  config = mkIf cfg.enable {
    xdg = {
      # commented out since 23.11 changes
      #portal = {
      #  enable = true;
      #  wlr.enable = true;
      #  extraPortals = [ pkgs.xdg-desktop-portal-gtk ];
      #};
      mime = {
        enable = true;
        #defaultApplications = ;
      };
      icons.enable = true;
      menus.enable = true;
    };
  };
}
