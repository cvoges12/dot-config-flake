{ config, lib, pkgs, ... }:

with lib;

let
  cfg = config.localModules.neovim;
in {
  options.localModules.neovim = {
    enable = mkEnableOption (mdDoc "neovim");
  };

  config = mkIf cfg.enable {
    programs.neovim = {
      enable = true;
      configure.plugins = {
        start = with pkgs.vimPlugins; [
          vim-one
        ] ++ lists.optional (pkgs.vimUtils.buildVimPluginFrom2Nix {
          name = "vim-colemak.lua";
          src = pkgs.fetchFromGitLab {
            owner = "cvoges12";
            repo = "vim-colemak.lua";
            rev = "01991e12b7589f09540a13a1b1bc86f286cd18f5";
            sha256 = "Sd0vyiuMJplSOoPJZxrnQ5a/TFI1FR0kO2IzDSQtcko=";
          };
        });
      };
    };
  };
}
