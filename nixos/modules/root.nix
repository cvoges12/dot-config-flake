{ config, lib, ... }:

with lib;

let
  cfg = config.localModules.root;
in {
  options.localModules.root = {
    enable = mkEnableOption (mdDoc "root");
    keys = mkOption {
      type = with types; listOf str;
      default = [];
    };
    hashedPassword = mkOption {
      type = types.str;
      default = "";
    };
  };

  config = mkIf cfg.enable {
    users.users.root = {
      openssh.authorizedKeys.keys = mkIf (cfg.keys != []) cfg.keys;
      hashedPassword = mkIf (cfg.hashedPassword != "") cfg.hashedPassword;
    };
  };
}
