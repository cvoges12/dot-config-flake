{ config, pkgs, lib, ... }:

with lib;

let
  cfg = config.localModules.shell.fish;
in {
  options.localModules.shell.fish = {
    enable = mkEnableOption (mdDoc "fish");
  };

  config = mkIf cfg.enable {
    programs.fish.enable = true;
    localModules.user.shell = pkgs.fish;
  };
}
