{ config, lib, pkgs, ... }:

with lib;

let
  cfg = config.localModules.wm.i3;
in {
  options.localModules.wm.i3.enable = mkEnableOption (
    mdDoc "i3"
  );

  config = mkIf cfg.enable {
    services.xserver = {
      enable = true;

      windowManager.i3.enable = true;

      displayManager = {
        defaultSession = "none+i3";
        lightdm = {
          enable = true;
          greeters.gtk = {
            enable = true;
            theme = {
              name = "WhiteSurTheme";
              package = pkgs.whitesur-gtk-theme;
            };
            iconTheme = {
              name = "WhiteSurIcons";
              package = pkgs.whitesur-icon-theme;
            };
            #cursorTheme = {
            #  name = ;
            #  package = ;
            #  size = 16;
            #};
          };
        };
      };

      desktopManager.xterm.enable = false;

      libinput = {
        enable = true;
        touchpad.tapping = false;
      };
      synaptics.enable = false;

      xkbOptions = "altwin:swap_lalt_lwin";
    };
  };
}
