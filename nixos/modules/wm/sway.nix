{ config, pkgs, lib, ... }:

with lib;

let
  cfg = config.localModules.wm.sway;
in {
  options.localModules.wm.sway.enable = mkEnableOption (
    mdDoc "sway"
  );

  config = mkIf cfg.enable {
    #programs.sway = {
    #  enable = true;
    #  wrapperFeatures.gtk = true;
    #};

    environment.systemPackages = with pkgs; [
      wl-clipboard
    ];
  };
}
