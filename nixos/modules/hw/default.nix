{ ... }:

{
  imports = [
    ./thinkpad-x220.nix
  ];

  boot.loader = {
    efi.canTouchEfiVariables = true;
    grub = {
      efiSupport = true;
      device = "nodev";
    };
  };
}
