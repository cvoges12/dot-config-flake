{ config, pkgs, lib, modulesPath, inputs, ... }:

with lib;

let
  cfg = config.localModules.hw.thinkpad-x220;
in {
  imports = [
    #inputs.nixos-hardware.nixosModules.lenovo-thinkpad-x220
    (modulesPath + "/installer/scan/not-detected.nix")
  ];

  options.localModules.hw.thinkpad-x220.enable = mkEnableOption (
    mdDoc "thinkpad x220"
  );

  config = mkIf cfg.enable {
    boot = {
      initrd.availableKernelModules = [
        "ehci_pci"
        "ahci"
        "usb_storage"
        "sd_mod"
        "sdhci_pci"
      ];
      kernelModules = [ "kvm-intel" ];
      kernelParams = [
        "quiet"
        "splash"
        "acpi_osi="
        #"acpi_osi=Linux"
        #"acpi_backlight=vendor"
      ];
    };

    nixpkgs = {
      hostPlatform = mkDefault "x86_64-linux";

      #config.packageOverrides = pkgs: {
      #  vaapiIntel = pkgs.vaapiIntel.override { enableHybridCodec = true; };
      #};
    };

    hardware = {
      cpu.intel.updateMicrocode = true;
      enableAllFirmware = true;

      #opengl = {
      #  enable = true;
      #  extraPackages = with pkgs; [
      #    intel-media-driver # LIBVA_DRIVER_NAME=iHD
      #    vaapiIntel         # LIBVA_DRIVER_NAME=i965 (older but works better for Firefox/Chromium)
      #    vaapiVdpau
      #    libvdpau-va-gl
      #  ];
      #};
    };
    
    #environment.sessionVariables.LIBVA_DRIVER_NAME = "i965";
    
    services = {
      hdapsd.enable = false;
      tlp.enable = true;
    };
  };
}
