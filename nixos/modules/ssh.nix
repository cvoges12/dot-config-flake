{ config, lib, ... }:

with lib;

let
  cfg = config.localModules.ssh;
in {
  options.localModules.ssh = {
    enable = mkEnableOption (mdDoc "batteries included ssh");
    ports = mkOption {
      type = types.listOf types.port;
      default = [ 22 ];
    };
    rootLogin = mkOption {
      type = types.str;
      default = "yes";
    };
    sftp = mkOption {
      type = types.bool;
      default = true;
    };
  };

  config = mkIf cfg.enable {
    services.openssh = {
      inherit (cfg) ports;

      enable = true;
      settings.PermitRootLogin = cfg.rootLogin;
      allowSFTP = cfg.sftp;
    };
  };
}
