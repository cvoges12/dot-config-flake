{ config, pkgs, lib, inputs, ... }:

let
  cfg = config.localModules.command-not-found;
in {
  options.localModules.command-not-found = {
    enable = lib.mkEnableOption (lib.mdDoc "command not found");
  };
  
  config = lib.mkIf cfg.enable {
    programs.command-not-found = {
      enable = true;
      dbPath = inputs.flake-programs-sqlite.packages.${pkgs.system}.programs-sqlite;
    };
  };
}