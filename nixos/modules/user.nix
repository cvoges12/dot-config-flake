{ config, pkgs, lib, ... }:

with lib;

let
  cfg = config.localModules.user;
in {
  options.localModules.user = {
    enable = mkEnableOption (mdDoc "user");
    name = mkOption {
      type = types.str;
      default = "";
      example = "player1";
    };
    keys = mkOption {
      type = with types; listOf str;
      default = [];
    };
    password = mkOption {
      type = types.str;
      default = "";
    };
    hashedPassword = mkOption {
      type = types.str;
      default = "";
    };
    shell = mkOption {
      type = types.package;
      default = pkgs.bashInteractive;
      example = pkgs.fish;
    };
  };

  config = mkIf cfg.enable {
    users.users.${cfg.name} = {
      inherit (cfg) shell;

      isNormalUser = true;
      description = cfg.name;
      packages = with pkgs; [ home-manager ];
      openssh.authorizedKeys.keys = cfg.keys;
      password = mkIf (cfg.password != "") cfg.password;
      hashedPassword = mkIf (cfg.hashedPassword != "") cfg.hashedPassword;
      extraGroups = [
        "wheel"
        "video"
        "libvirtd"
        "systemd-journal"
        "kvm"
        "render"

	      # remove for next deployment
        "disk"
      ];
    };
  };
}
