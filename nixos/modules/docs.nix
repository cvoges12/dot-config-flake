{ config, lib, pkgs, ... }:

with lib;

let
  cfg = config.localModules.docs;
in {
  options.localModules.docs = {
    enable = mkEnableOption (mdDoc "full documentation");
  };

  config = mkIf cfg.enable {
    environment.systemPackages = with pkgs; [
      man-pages
      man-pages-posix
    ];

    documentation = {
      enable = true;
      man.enable = true;
      nixos.enable = true;
      dev.enable = true;
      doc.enable = true;
      info.enable = true;
    };
  };
}
