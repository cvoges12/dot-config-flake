#!/bin/sh

flake=$1

nix --extra-experimental-features "nix-command  flakes" run github:nix-community/disko -- --mode disko --flake /home/nixos/.config/flake#"$flake"
nixos-install --no-root-passwd --flake /home/nixos/.config/flake#"$flake"