{ pkgs, ... }:

{
  localModules = {
    bat.enable = true;
    bitwarden.enable = true;
    btop.enable = true; # make top variable to pass to wm
    git.enable = true;
    gui.enable = true;
    kitty.enable = true;
    mpv.enable = true;
    neovim.enable = true;
    nix.enable = true;
    obs.enable = true;
    syncthing.enable = true;

    keymap = {
      enable = true;
      variant = "colemak";
    };

    wm = {
      enable = true;
      #sway.enable = true;
      i3.enable = true;
    };

    #firefox = {
    #  enable = true;
    #  addons.enable = true;
    #  settings.enable = true;
    #  #clearBrowser.enable = true;
    #};

    vscode = {
      enable = true;
      ai = true;
      c = true;
      git = true;
      lua = true;
      markup = true;
      nvim = true;
      py = true;
      rust = true;
      sh = true;
      texmd = true;
      web = true;
    };

    user = {
      enable = true;
      name = "cvoges12";
      wallpaper = pkgs.fetchurl {
        url = "https://images.unsplash.com/photo-1516655855035-d5215bcb5604";
        sha256 = "sha256-E04mpG32op8tCgy0OjlbNKbRwufPjQuwv9I0vKm6TbE=";
      };
      packages = with pkgs; [
        # audio controls
        pamixer
        pavucontrol
        qpwgraph
        easyeffects
  
        # music
        #ardour  # daw
        #musescore   # music score
        caudec  # audio file conversion

        # used for media buttons
        playerctl
  
        asciinema
        bat
        debootstrap
        entr
        weechat
        ffmpeg
        harfbuzz
        hunspell
        neofetch
        signal-desktop
        strace
        tldr
        #virtmanager
        #qemu
  
        # art programs
        inkscape
        gimp
        krita
        luminanceHDR
        olive-editor
        openscad
        darktable
        # incase i don't like darktable
        #digikam
        blender
  
        obsidian
        libreoffice

        # used for xdg-open
        # so apps can open default browser and such
        xdg-utils
      ];
    };

    shell = {
      enable = true;
      fish = {
        enable = true;
        functions = {
          gitignore = "curl -sL https://www.gitignore.io/api/$argv";
          nixlint = "nix run github:astro/deadnix $argv; nix run github:nerdypepper/statix check $argv";
          loc = "find $argv[1] -regextype posix-extended -regex \".*\.($argv[2])\" | xargs wc -l | sort -hr | head -n 1";
          pastebin = "curl -F\"file=@$argv[1]\" https://0x0.st";
        };
      };
      aliases = {
        cat = "bat";
        g = "git";
        gputop = "intel_gpu_top";
        bitwarden = "rbw";
        battery = "upower -i /org/freedesktop/UPower/devices/battery_BAT0";

        ss = "rm ~/Downloads/screenshot.png; if [ $XDG_SESSION_TYPE = \"wayland\" ]; grim ~/Downloads/screenshot.png; else; scrot -F ~/Downloads/screenshot.png; end";
        sss = "rm ~/Downloads/screenshot.png; if [ $XDG_SESSION_TYPE = \"wayland\" ]; grim -g \"$(slurp)\" ~/Downloads/screenshot.png; else; scrot -s -F ~/Downloads/screenshot.png; end";
        
        tree = "eza --color always --icons --tree";
        l = "eza --color always --icons --tree --level=1";
        ll = "eza --color always --icons --tree --level=1 --long --header --group --inode --links --blocks";
        la = "eza --color always --icons --tree --level=1 --long --header --group --inode --links --blocks --all";
        
        sc = "su -c";
        spc = "su --preserve-environment -c";
        
        ns = "su -c \"nixos-rebuild switch --flake path:/home/cvoges12/.config/flake/#$(hostname)\"";
        hs = "if [ $USER != \"root\" ]; home-manager switch --flake path:/home/cvoges12/.config/flake/#$(hostname); end";
        disko = "su -c \"nix run github:nix-community/disko -- -m disko --flake path:/home/cvoges12/.config/flake/#$(hostname)\"";
        
        viflake = "cd ~/.config/flake; nvim flake.nix";
        vios = "cd ~/.config/flake/nixos/; nvim";
        vihome = "cd ~/.config/flake/home-manager/; nvim";
        
        lan = "ping $(ip route | sed -nE 's/default via ([0-9]+\\.[0-9]+\\.[0-9]+\\.[0-9]+).*/\\1/p')";
        wan = "ping 9.9.9.9";
        high-ping = "sed -nE 's/^64.*time=([0-9]{3,}).*ms$/HIGH PING: \\1 ms/p'";

        py = "nix-shell -p python3 --command python";
        js = "nix-shell -p nodejs --command node";
      };
    };
  };

  programs = {
    brave.enable = true;
    ssh.enable = true;
    texlive.enable = true;
    zathura.enable = true;
  };
}
