{ config, lib, ... }:

with lib;

let
  cfg = config.localModules.alacritty;
in {
  options.localModules.alacritty = {
    enable = mkEnableOption (mdDoc "alacritty");
  };

  config = mkIf cfg.enable {
    programs.alacritty = {
      enable = true;

      # no ligature support
      # font size ignored
      # no opacity respected
      settings = {
        selection.save_to_clipboard = true;
        window.dimensions = {
          lines = 20;
          columns = 84; # for number line
          opacity = "0.6";
        };
        font = {
          size = 10;
          normal = {
            family = "JetBrainsMono Nerd Font Mono";
            style = "Medium";
          };
          bold = {
            family = "JetBrainsMono Nerd Font Mono";
            style = "Bold";
          };
          italics = {
            family = "JetBrainsMono Nerd Font Mono";
            style = "MediumItalics";
          };
          bold_italics = {
            family = "JetBrainsMono Nerd Font Mono";
            style = "BoldItalics";
          };
        };
        colors = {
          primary = {
            background = "#282a36";
            foreground = "#f8f8f2";
            bright_foreground = "#ffffff";
          };
          cursor = {
            text = "CellBackground";
            cursor = "CellForeground";
          };
          vi_mode_cursor = {
            text = "CellBackground";
            cursor = "CellForeground";
          };
          search = {
            matches = {
              foreground = "#44475a";
              background = "#50fa7b";
            };
            focused_match = {
              foreground = "#44475a";
              background = "#ffb86c";
            };
            footer_bar = {
              background = "#282a36";
              foreground = "#f8f8f2";
            };
          };
          hints = {
            start = {
              foreground = "#282a36";
              background = "#f1fa8c";
            };
            end = {
              foreground = "#f1fa8c";
              background = "#282a36";
            };
          };
          line_indicator = {
            foreground = "None";
            background = "None";
          };
          selection = {
            text = "CellForeground";
            background = "#44475a";
          };
          normal = {
            black = "#21222c";
            red = "#ff5555";
            green = "#50fa7b";
            yellow = "#f1fa8c";
            blue = "#bd93f9";
            magenta = "#ff79c6";
            cyan = "#8be9fd";
            white = "#f8f8f2";
          };
          bright = {
            black = "#6272a4";
            red = "#ff6e6e";
            green = "#69ff94";
            yellow = "#ffffa5";
            blue = "#d6acff";
            magenta = "#ff92df";
            cyan = "#a4ffff";
            white = "#ffffff";
          };
        };
      };
    };
  };
}
