{ config, lib, inputs, ... }:

with lib;

let
  cfg = config.localModules.nix;
in {
  options.localModules.nix = {
    enable = mkEnableOption (mdDoc "nix");
  };

  config = mkIf cfg.enable {
    nixpkgs = {
      config.allowUnfreePredicate = (_: true);
      overlays = [
        inputs.nur.overlay
        (final: prev: {
          unstable = import inputs.nixpkgs-unstable {
            inherit (config.nixpkgs) config system;
          };
        })
      ];
    };
  };
}
