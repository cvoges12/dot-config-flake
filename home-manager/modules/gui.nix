{ config, lib, pkgs, ... }:

with lib;

let
  cfg = config.localModules.gui;
in {
  options.localModules.gui = {
    enable = mkEnableOption (mdDoc "gtk and qt");
  };

  config = mkIf cfg.enable {
    gtk = {
      enable = true;
      iconTheme = {
        name = "WhiteSurIcons";
        package = pkgs.whitesur-icon-theme;
      };
      theme = {
        name = "WhiteSurTheme";
        package = pkgs.whitesur-gtk-theme;
      };
      gtk3.extraConfig = {
        gtk-application-prefer-dark-theme = true;
        gtk-recent-files-limit = 20;
      };
      gtk4.extraConfig = {
        gtk-application-prefer-dark-theme = true;
        gtk-recent-files-limit = 20;
      };
    };

    qt.platformTheme = "gtk";
  };
}
