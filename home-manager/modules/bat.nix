{ config, lib, ... }:

with lib;

let
  cfg = config.localModules.bat;
in {
  options.localModules.bat = {
    enable = mkEnableOption (mdDoc "bat");
  };

  config = mkIf cfg.enable {
    programs.bat = {
      enable = true;
      config = {
        pager = "less -FR";
        theme = "ansi";
      };
    };
  };
}
