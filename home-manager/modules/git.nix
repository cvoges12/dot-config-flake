{ config, pkgs, lib, ... }:

with lib;

let
  cfg = config.localModules.git;
in {
  options.localModules.git = {
    enable = mkEnableOption (mdDoc "git");
  };

  config = mkIf cfg.enable {
    programs.git = {
      enable = true;
      package = pkgs.gitFull;
      userEmail = if config.home.username == "cvoges12" then
        "vogesclayton@gmail.com"
      else
        null;
      userName = config.home.username;

      # find preference
      #delta.enable = true;
      #diff-so-fancy.enable = true;
      difftastic.enable = true;

      aliases = {
      };

      extraConfig = {
        core.editor = "nvim";
        credential.helper = "cache";
      };
    };
  };
}
