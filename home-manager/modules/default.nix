{ ... }:

{
  imports = [
    ./alacritty.nix
    ./bat.nix
    ./bitwarden.nix # abstract out email
    ./btop.nix
    ./email.nix # abstract out email
    ./firefox.nix # fixed but add some more options
    ./git.nix
    ./gui.nix
    ./htop.nix
    ./kitty.nix
    ./keymap.nix
    ./mpd.nix
    ./mpv.nix
    ./neovim.nix
    ./newsboat.nix
    ./nix.nix
    ./obs.nix
    ./qutebrowser.nix
    ./shell
    ./syncthing.nix
    ./user.nix
    ./vscode.nix
    ./wm
  ];
}
