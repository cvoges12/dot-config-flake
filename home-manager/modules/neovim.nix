{ config, pkgs, lib, ... }:

with lib;

let
  cfg = config.localModules.neovim;
in {
  options.localModules.neovim = {
    enable = mkEnableOption (mdDoc "neovim");
  };

  config = mkIf cfg.enable {
    programs.neovim = {
      enable = true;
      defaultEditor = true;
      extraLuaConfig = ''
        vim.g.mapleader = ' '
        vim.o.mouse = ""
        vim.bo.fileformat = 'unix'
        vim.o.hidden = true
      '' + strings.optionalString (config.localModules.keymap.variant == "colemak") ''
        local colemak = require('colemak')
        colemak.map()
        
        -- Search
        if vim.g.vscode then
        else
          colemak.keys_modes_map(
            {'n','v'},
            {'/',  '?'},
            {'q/i','q?i'}
          )
        end
        
        -- Remove left over search highlights
        vim.api.nvim_set_keymap(
          'n',
          '<Leader><space>',
          ':nohlsearch<CR>',
          {noremap = true}
        )
        
        -- Move current line to center of screen
        vim.api.nvim_set_keymap(
          'n',
          '<Leader>m',
          'z.',
          {noremap = true}
        )
        
        -- Buffers
        local buffer_keys = {
          new = { '<tab>','<S-tab>','<BS>'},
          old = { 'n',    'p',     'd'}
        }
        for i = 1, #buffer_keys.new do
          for _,f in pairs(
            colemak.modes_map(
              {'n','v','x'}
            )
          ) do
            f(
              '<Leader>'..buffer_keys.new[i],
              ':b'      ..buffer_keys.old[i]..'<CR>',
              {noremap = true}
            )
          end
        end
        
        -- Command mode
        if vim.g.vscode then
        else
          colemak.keys_modes_map(
            {'n','v','x'},
            {':',  ';;',  ';e',          ';h',           ';v'},
            {'q:i','q:i!','q:ie term://','q:isp term://','q:ivs term://'}
          )
        end
      '';

      # Add:
      # https://github.com/vijaymarupudi/nvim-fzf
      plugins = mkIf (config.localModules.keymap.variant == "colemak") [
        (pkgs.vimUtils.buildVimPlugin {
          name = "neovim-colemak.lua";
          src = pkgs.fetchFromGitLab {
            owner = "cvoges12";
            repo = "neovim-colemak.lua";
            rev = "01991e12b7589f09540a13a1b1bc86f286cd18f5";
            sha256 = "Sd0vyiuMJplSOoPJZxrnQ5a/TFI1FR0kO2IzDSQtcko=";
          };
          # uncomment at next release
          #src = pkgs.fetchGit {
          #  url = "https://gitlab.com/cvoges12/vim-colemak.lua";
          #  ref = "main";
          #  rev = "01991e12b7589f09540a13a1b1bc86f286cd18f5";
          #};
        })
      ];
    };
  };
}
