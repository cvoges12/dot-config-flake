{ config, lib, ... }:

with lib;

let
  cfg = config.localModules.btop;
in {
  options.localModules.btop = {
    enable = mkEnableOption (mdDoc "btop");
  };

  config = mkIf cfg.enable {
    programs.btop = {
      enable = true;

      # https://github.com/aristocratos/btop#configurability
      settings = {
        # general #
        color_theme = "dracula";
        theme_background = true;
        truecolor = true;
        force_tty = false;
        vim_keys = false;
        presets = "cpu:1:default,proc:0:default cpu:0:default,mem:0:default,net:0:default cpu:0:block,net:0:tty";
        shown_boxes = "proc cpu mem net";
        update_ms = 2000;
        rounded_corners = true;
        graph_symbol = "braille";
        clock_format = "%H:%M:%S";
        base_10_sizes = false;
        background_update = true;
        show_battery = true;
        selected_battery = "Auto";
        log_level = "WARNING";

        # cpu #
        cpu_bottom = false;
        graph_symbol_cpu = "default";
        cpu_graph_upper = "user";
        cpu_graph_lower = "total";
        cpu_invert_lower = true;
        cpu_single_graph = false;
        check_temp = true;
        cpu_sensor = "Auto";
        show_coretemp = true;
        cpu_core_map = "";
        temp_scale = "celsius";
        show_cpu_freq = true;
        custom_cpu_name = "";
        show_uptime = true;

        # mem #
        mem_below_net = false;
        graph_symbol_mem = "default";
        mem_graphs = true;
        show_disks = true;
        show_io_stat = true;
        io_mode = false;
        io_graph_combined = false;
        io_graph_speeds = "";
        show_swap = true;
        swap_disk = true;
        only_physical = false;
        use_fstab = true;
        zfs_hide_datasets = false;
        disk_free_priv = false;
        disks_filter = "exclude=";
        zfs_arc_cached = false;

        # net #
        graph_symbol_net = "default";
        net_download = 1;
        net_upload = 1;
        net_auto = false;
        net_sync = true;
        net_iface = "";

        # proc #
        proc_left = false;
        graph_symbol_proc = "default";
        proc_sorting = "cpu lazy";
        proc_reversed = false;
        proc_tree = false;
        proc_colors = true;
        proc_gradient = true;
        proc_per_core = true;
        proc_mem_bytes = true;
        proc_cpu_graphs = true;
        proc_info_smaps = false;
        proc_filter_kernel = false;
      };
    };
  };
}
