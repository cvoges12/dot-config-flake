{ config, lib, pkgs, ... }:

with lib;

let
  cfg = config.localModules.syncthing;
in {
  options.localModules.syncthing = {
    enable = mkEnableOption (mdDoc "syncthing");
  };

  config = mkIf cfg.enable {
    home.packages = [ pkgs.syncthingtray ];
    services.syncthing = {
      enable = true;
      tray = {
        #enable = true;
        package = pkgs.syncthingtray;
        command = "syncthingtray --wait";
      };
    };
  };
}
