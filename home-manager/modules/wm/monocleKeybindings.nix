{ lib, wm, msg, menu, ... }:

with lib;

(
  # change focus
  attrsets.mapAttrs'
    (name: value: attrsets.nameValuePair 
      "${wm.mod}+${value}"
      "exec ${msg} fullscreen disable; exec ${msg} focus ${name}; exec ${msg} fullscreen enable")
    { inherit (wm) left right up down; }
) // (
  # move
  attrsets.mapAttrs'
    (name: value: attrsets.nameValuePair 
      "${wm.mod}+Shift+${value}"
      "exec ${msg} fullscreen disable; exec ${msg} mode default; exec ${msg} move ${name}'")
    { inherit (wm) left right up down; }
) // {
  "${wm.mod}+m" = "exec ${msg} fullscreen disable; exec ${msg} mode default";
  "Escape" = "exec ${msg} fullscreen disable; exec ${msg} mode default";
}
