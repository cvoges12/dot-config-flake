{ config, pkgs, lib, ... }:

with lib;

let
  inherit (config.xdg) dataHome;
  inherit (config.localModules) wm;
  cfg = wm.i3;
in {
  options.localModules.wm.i3 = {
    enable = mkEnableOption (mdDoc "i3");

    msg = mkOption {
      type = types.str;
      default = "i3-msg";
    };

    menu = mkOption {
      type = types.str;
      default = "${pkgs.dmenu}/bin/dmenu_run";
    };
  };

  config = mkIf cfg.enable {
    home.packages = with pkgs; [
      xorg.xinit
      i3lock
      arandr

      # clipboard
      xclip
      
      # notifications
      deadd-notification-center
      libnotify # for `notify-send`
      
      # menu
      dmenu
      
      # screenshots
      scrot
    ];

    programs.feh.enable = true;

    xsession = {
      enable = true;
      windowManager.i3 = {
        enable = true;

        config = {
          inherit (wm) terminal;
          inherit (cfg) menu;

          modifier = wm.mod;
          defaultWorkspace = wm.ws.grave;

          bars = [];

          startup = [
            { command = "${pkgs.networkmanagerapplet}/bin/nm-applet --indicator"; }
            {
              command = "${pkgs.feh}/bin/feh --bg-fill ${dataHome}/wallpaper.jpg";
              always = true;
              notification = false;
            }
            {
              command = "systemctl --user restart polybar.service";
              always = true;
              notification = false;
            }
          ];

          gaps = {
            inner = 10;
            smartBorders = "no_gaps";
            smartGaps = true;
          };

          window = {
            titlebar = false;
            border = 1;
            hideEdgeBorders = "smart";
            commands = [
              {
                command = "resize set 680 420";
                criteria = { class = "^floating-kitty.*$"; };
              }
            ];
          };

          floating = {
            titlebar = false;
            border = 1;

            criteria = [
              { class = "^(?!(B|b)rave|(S|s)ignal|(C|c)ode|kitty).*$"; }
            ];
          };

          colors = {
            focused = {
              background = "#0c0c0c";
              border = "#000000";
              childBorder = "#0c0c0c";
              indicator = "#000000";
              text = "#ffffff";
            };

            focusedInactive = {
              background = "#5f676a";
              border = "#333333";
              childBorder = "#5f676a";
              indicator = "#484e50";
              text = "#ffffff";
            };

            placeholder = {
              background = "#0c0c0c";
              border = "#000000";
              childBorder = "#0c0c0c";
              indicator = "#000000";
              text = "#ffffff";
            };

            unfocused = {
              background = "#222222";
              border = "#333333";
              childBorder = "#222222";
              indicator = "#292d2e";
              text = "#888888";
            };

            urgent = {
              background = "#900000";
              border = "#900000";
              childBorder = "#900000";
              indicator = "#900000";
              text = "#ffffff";
            };
          };
          
          ### ABOVE GOES CODE IN BOTH I3 AND SWAY ###

          keybindings = ((import ./defaultKeybindings.nix) {
            inherit lib wm;
            inherit (cfg) msg menu;
          }) // {
            "${wm.mod}+b" = "exec polybar-msg cmd toggle";

            #"${wm.mod}+Shift+Escape" = "exec wlogout"; # find logout manager
            "${wm.mod}+Shift+r" = "exec ${cfg.msg} restart; polybar-msg cmd restart"; #; sway-client -R"; # find notification center
  
            "${wm.mod}+Tab" = "exec kill -s USR1 $(pidof deadd-notification-center)";
            "${wm.mod}+BackSpace" = "exec notify-send --hint boolean:deadd-notification-center:true string:type:clearInCenter";
            "${wm.mod}+Shift+BackSpace" = "exec notify-send --hint boolean:deadd-notification-center:true string:type:clearPopups";
          };

          modes = {
            monocle = config.xsession.windowManager.i3.config.keybindings
              // ((import ./monocleKeybindings.nix) {
                inherit lib wm;
                inherit (cfg) msg menu;
              });
            resize = config.xsession.windowManager.i3.config.keybindings
              // ((import ./resizeKeybindings.nix) {
                inherit wm;
              });
          };
        };
      };
    };

    services = {
      picom.enable = true;

      screen-locker = {
        enable = true;
        
        lockCmd = "${pkgs.i3lock}/bin/i3lock -n -d -i ${dataHome}/wallpaper.jpg";
        
        inactiveInterval = 30;
        xss-lock.screensaverCycle = 120;
      };
      
      polybar = {
        enable = true;
        package = pkgs.polybar.override {
          alsaSupport = true;
          githubSupport = true;
          mpdSupport = true;
          pulseSupport = true;
          iwSupport = true;
          nlSupport = true;
          i3Support = true;
        };

        config = {
          "global/wm" = {
            margin-bottom = 0;
            margin-top = 0;
          };

          "settings" = {
            compositing-background = "source";
            compositing-foreground = "over";
            compositing-overline = "over";
            compositing-underline = "over";
            compositing-border = "over";
          };

          "bar/top" = {
            enable-ipc = true;

            #offset-y = "-2"; # does not work on i3
            border-size = "1%";
            border-color = "#00000000";
            width = "100%";
            height = 20;
            radius = 0;
            #margin = 0; # can't figure it out

            line-size = 1;

            background = "#00000000";

            font-0 = "Noto Sans Nerd Font Propo:size=10;1";
            font-1 = "Noto Sans Nerd Font Propo:style=Medium:size=11;3";

            fixed-center = true;
            #module-margin-left = 2;
            #module-margin-right = 2;

            modules-left = "left cpu mem temp tray window right";
            modules-center = "left workspaces right";
            modules-right = "left notif pulse light batt date right";

            scroll-up = "#workspaces.prev";
            scroll-down = "#workspaces.next";
          };

          "glyph" = {
            type = "custom/text";
            content-foreground = "#0c0c0c";
            content-font = 2;
          };

          "module/left" = {
            "inherit" = "glyph";
            content = "";
          };

          "module/right" = {
            "inherit" = "glyph";
            content = "";
          };

          "module/cpu" = {
            type = "internal/cpu";
            interval = 5;

            warn-percentage = 75;
            label = "%percentage%% ";
            format = "<label>";
            format-background = "#0c0c0c";
            format-padding = 2;

            label-warn = "%percentage%% ";
            format-warn = "<label-warn>";
            format-warn-background = "#0c0c0c";
            format-warn-padding = 2;
          };

          "module/mem" = {
            type = "internal/memory";
            interval = 5;

            warn-percentage = 75;

            label = "%percentage_used%% ";
            format = "<label>";
            format-background = "#0c0c0c";
            format-padding = 2;

            label-warn = "%percentage_used%% ";
            format-warn = "<label-warn>";
            format-warn-background = "#0c0c0c";
            format-warn-padding = 2;
          };

          "module/temp" = {
            type = "internal/temperature";
            interval = 5;

            warn-temperature = 75;
            units = true;
            label = "%temperature-c% ";
            format = "<label>";
            format-background = "#0c0c0c";
            format-padding = 2;

            label-warn = "%temperature-c% ";
            format-warn = "<label-warn>";
            format-warn-background = "#0c0c0c";
            format-warn-padding = 2;
          };

          # in next polybar version
          "module/tray" = {
            type = "internal/tray";

            format = "<tray>";
            format-background = "#0c0c0c";
            format-padding = 2;
          };

          "module/window" = {
            type = "internal/xwindow";
            interval = 1;

            label = "%title%";
            label-maxlen = 64;
            format = "<label>";
            format-background = "#0c0c0c";
            format-padding = 2;
          };

          "module/workspaces" = {
            type = "internal/i3";

            #scroll-up = "i3wm-wmnext";
            #scroll-down = "i3wm-wmprev";

            pin-workspaces = true;
            show-urgent = true;
            index-sort = true;

            label-mode = "%mode%";
            label-mode-padding = 2;
            label-mode-background = "#e60053";

            label-focused = "%name%";
            label-focused-underline = "#ffffff";
            label-focused-padding = 2;
            label-focused-foreground = "#ffffff";
            label-focused-background = "#0c0c0c";

            label-unfocused = "%name%";
            label-unfocused-padding = 2;
            label-unfocused-background = "#0c0c0c";

            label-visible = "%name%";
            label-visible-underline = "#ffffff";
            label-visible-padding = 2;
            label-visible-foreground = "#ffffff";
            label-visible-background = "#0c0c0c";

            label-urgent = "%name%";
            label-urgent-padding = 2;
            label-urgent-background = "#900000";

            format = "<label-state><label-mode>";
            format-background = "#0c0c0c";
          };

          # https://github.com/anufrievroman/polytiramisu

          "module/pulse" = {
            type = "internal/pulseaudio";
            interval = 1;

            ramp-volume-0 = "";
            ramp-volume-1 = "";
            ramp-volume-2 = "";
            format-volume = "<label-volume> <ramp-volume>";
            format-volume-background = "#0c0c0c";
            format-volume-padding = 2;

            label-muted = "󰝟";
            format-muted = "<label-muted>";
            format-muted-background = "#0c0c0c";
            format-muted-padding = 2;

            click-left = "pavucontrol";
          };

          # https://github.com/rototrash/polybar-mic

          "module/light" = {
            type = "internal/backlight";
            interval = 1;
            card = "acpi_video0";

            label = "%percentage%%";
            ramp-0 = "";
            ramp-1 = "";
            ramp-2 = "";
            ramp-3 = "";
            ramp-4 = "";
            ramp-5 = "";
            ramp-6 = "";
            ramp-7 = "";
            ramp-8 = "";
            format = "<label> <ramp>";
            format-background = "#0c0c0c";
            format-padding = 2;
          };

          "module/batt" = {
            type = "internal/battery";
            interval = 1;

            label-charging = "%percentage%% ";
            format-charging = "<label-charging>";
            format-charging-background = "#0c0c0c";
            format-charging-padding = 2;

            label-discharging = "%percentage%%";
            ramp-capacity-0 = "";
            ramp-capacity-1 = "";
            ramp-capacity-2 = "";
            ramp-capacity-3 = "";
            ramp-capacity-4 = "";
            format-discharging = "<label-discharging> <ramp-capacity>";
            format-discharging-background = "#0c0c0c";
            format-discharging-padding = 2;

            label-full = "%percentage%% ";
            format-full = "<label-full>";
            format-full-background = "#0c0c0c";
            format-full-padding = 2;
          };

          "module/date" = {
            type = "internal/date";
            interval = 5;

            date = "";
            time = "%H:%M";
            date-alt = "%Y-%m-%d%";
            time-alt = "";
            label = "%date%%time%";

            format-background = "#0c0c0c";
            format-padding = 2;
          };
        };

        # https://github.com/polybar/polybar/issues/763
        script = ''
          for monitor in $(polybar --list-monitors | ${pkgs.coreutils}/bin/cut -d ":" -f 1); do
            MONITOR=$monitor polybar --reload top &
          done
        '';
      };
    };
  };
}
