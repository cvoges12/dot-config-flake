{ wm, ... }:

{
  "${wm.mod}+r" = "mode default";

  "${wm.mod}+${wm.left}" = "resize shrink width ${wm.p} px or ${wm.p} ppt";
  "${wm.mod}+${wm.down}" = "resize grow height ${wm.p} px or ${wm.p} ppt";
  "${wm.mod}+${wm.up}" = "resize shrink height ${wm.p} px or ${wm.p} ppt";
  "${wm.mod}+${wm.right}" = "resize grow width ${wm.p} px or ${wm.p} ppt";

  "${wm.mod}+Shift+${wm.left}" = "resize shrink width 10 px or 10 ppt";
  "${wm.mod}+Shift+${wm.down}" = "resize grow height 10 px or 10 ppt";
  "${wm.mod}+Shift+${wm.up}" = "resize shrink height 10 px or 10 ppt";
  "${wm.mod}+Shift+${wm.right}" = "resize grow width 10 px or 10 ppt";
}
