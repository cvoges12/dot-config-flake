{ config, lib, pkgs, ... }:

with lib;

let
  cfg = config.localModules.wm;
  inherit (config.localModules.keymap) variant;
in {
  imports = [
    ./i3.nix
    ./sway.nix
  ];

  options.localModules.wm = {
    enable = mkEnableOption (mdDoc "wm");

    left = mkOption {
      type = types.str;
      default = "h";
    };
    down = mkOption {
      type = types.str;
      default = "j";
    };
    up = mkOption {
      type = types.str;
      default = "k";
    };
    right = mkOption {
      type = types.str;
      default = "l";
    };

    mod = mkOption {
      type = types.str;
      default = "Mod4";
    };

    ws = {
      grave = mkOption {
        type = types.str;
        default = "workspace 0";
      };
      "1" = mkOption {
        type = types.str;
        default = "workspace 1";
      };
      "2" = mkOption {
        type = types.str;
        default = "workspace 2";
      };
      "3" = mkOption {
        type = types.str;
        default = "workspace 3";
      };
      "4" = mkOption {
        type = types.str;
        default = "workspace 4";
      };
      "5" = mkOption {
        type = types.str;
        default = "workspace 5";
      };
      "6" = mkOption {
        type = types.str;
        default = "workspace 6";
      };
      "7" = mkOption {
        type = types.str;
        default = "workspace 7";
      };
      "8" = mkOption {
        type = types.str;
        default = "workspace 8";
      };
      "9" = mkOption {
        type = types.str;
        default = "workspace 9";
      };
      "0" = mkOption {
        type = types.str;
        default = "workspace 10";
      };
    };

    p = mkOption {
      type = types.str;
      default = "5"; # px or ppt
    };

    terminal = mkOption {
      type = types.str;
      default = "kitty";
    };
  };

  config = mkIf cfg.enable {
    services.network-manager-applet.enable = true;
  };
}
