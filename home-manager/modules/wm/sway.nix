{ config, pkgs, lib, ... }:

with lib;

let
  inherit (config.xdg) dataHome;
  inherit (config.localModules) wm;
  cfg = wm.sway;
in {
  options.localModules.wm.sway = {
    enable = mkEnableOption (mdDoc "sway");

    msg = mkOption {
      type = types.str;
      default = "swaymsg";
    };

    menu = mkOption {
      type = types.str;
      default = "fuzzel";
    };
  };

  config = mkIf cfg.enable {
    services.clipman = {
      enable = true;
      systemdTarget = "sway-session.target";
    };

    home = {
      file = {
        ".config/swaync/config.json".source = ./swaync.json;
        ".config/electron-flags.conf".text = ''
          --enable-features=WaylandWindowDecorations
          --ozone-platform-hint=auto
          --enable-webrtc-pipewire-capturer
          --gtk-version=4
        '';
        ## FOR OLD ##
        #--enable-features=UseOzonePlatform
        #--ozone-platform=wayland
      };

      pointerCursor = {
        name = "Adwaita";
        package = pkgs.gnome.adwaita-icon-theme;
        size = 24;
        x11 = {
          enable = true;
          defaultCursor = "Adwaita";
        };
      };

      sessionVariables = {
        _JAVA_AWT_WM_NONREPARENTING = 1;
        MOZ_ENABLE_WAYLAND = 1;
        MOZ_DRM_DEVICE = "/dev/dri/card0";
        MOZ_DBUS_REMOTE = 1;
        QT_WAYLAND_DISABLE_WINDOWDECORATION = 1;
        SDL_VIDEODRIVER = "wayland";
        XDG_SESSION_TYPE = "Wayland";
        QT_SCALE_FACTOR = 1;
        QT_QPA_PLATFORM = "wayland";
        GDK_BACKEND = "wayland";
        CLUTTER_BACKEND = "wayland";
      };

      packages = with pkgs; [
        wlogout
        wayvnc
        waynergy
        waypipe
        wdisplays
        waydroid
        qt6.qtwayland
        libsForQt5.qt5.qtwayland
        glfw-wayland
        wl-clipboard

        # notifications
        swaynotificationcenter

        # menu
        fuzzel

        # images
        swaybg
        imv

        # screenshots
        grim
        slurp

        # xwayland testing
        xorg.xwininfo
        xorg.xeyes
      ];
    };

    wayland.windowManager.sway = {
      enable = true;
  
      systemdIntegration = true;
      #systemd = {
      #  enable = true;
      #  xdgAutostart = true;
      #};
      wrapperFeatures = {
        base = true;
        gtk = true;
      };
      swaynag.enable = true;
      xwayland = true;
  
      config = {
        inherit (wm) terminal;
        inherit (cfg) menu;

        modifier = wm.mod;
        defaultWorkspace = wm.ws.grave;

        startup = [
          #{ command = "${wm.terminal}"; }
          { command = "${pkgs.networkmanagerapplet}/bin/nm-applet --indicator"; }
          { command = "${pkgs.syncthingtray}/bin/syncthingtray --wait"; }
        ];

        gaps = {
          inner = 10;
          smartBorders = "no_gaps";
          smartGaps = true;
        };

        window = {
          hideEdgeBorders = "smart";
          titlebar = false;
          commands = [
            {
              command = "resize set 450 400, move position 0 0";
              criteria = { title = ".*Syncthing Tray.*"; };
            }
          ];
        };

        floating = {
          border = 5;
          titlebar = false;

          criteria = [
            { title = ".*Syncthing Tray.*"; }
          ];
        };
  
  ### ABOVE CODE GOES IN BOTH I3 AND SWAY ###

        inherit (wm) left;
        inherit (wm) down;
        inherit (wm) up;
        inherit (wm) right;

        output."*".bg = "${dataHome}/wallpaper.jpg fill";

        bars = [];
        #bars = [
        #  {
        #    command = "${pkgs.waybar}/bin/waybar";
        #    mode = "dock";
        #    position = "top";
        #  }
        #];

        keybindings = ((import ./defaultKeybindings.nix) {
          inherit lib wm;
          inherit (cfg) msg menu;
        }) // {
          "${wm.mod}+b" = "exec ${cfg.msg} 'bar mode toggle'";

          "${wm.mod}+Shift+Escape" = "exec wlogout";
          "${wm.mod}+Shift+r" = "exec ${cfg.msg} 'reload'; swaync-client -R -rs";

          "${wm.mod}+Tab" = "exec swaync-client -t -sw";
          "${wm.mod}+Backspace" = "exec swaync-client -C -sw";
          "${wm.mod}+Shift+Backspace" = "exec swaync-client --hide-latest -sw";
        };

        modes = {
          monocle = config.wayland.windowManager.sway.config.keybindings
            // ((import ./monocleKeybindings.nix) {
              inherit lib wm;
              inherit (cfg) msg menu;
            });
          resize = config.wayland.windowManager.sway.config.keybindings
            // ((import ./resizeKeybindings.nix) {
              inherit wm;
            });
        };
      };
    };

    programs = {
      swaylock = {
        enable = true;
        settings = {
          image = "${dataHome}/wallpaper.jpg";
          font-size = 24;
          indicator-idle-visible = false;
          indicator-radius = 100;
          line-color = "ffffff";
          show-failed-attempts = true;
          daemonize = true;
        };
      };

      waybar = {
        enable = true;
        systemd = {
          enable = true;
          target = "sway-session.target";
        };
        settings.mainBar = {
          id = "bar-0";
          ipc = true;
          mode = "dock";
          position = "top";
          layer = "bottom";
          height = 24;
          spacing = 0;
          modules-left = [
            "cpu"
            "memory"
            "temperature"
            "tray"
            "sway/scratchpad"
            "sway/window"
          ];
          modules-center = [
            "sway/workspaces"
            "sway/mode"
          ];
          modules-right = [
            #"mpd"
            "custom/notification"
            "idle_inhibitor"
            "pulseaudio"
            "backlight"
            "battery"
            "clock"
          ];
          "sway/mode" = {
            format = "{}";
          };
          "sway/scatchpad" = {
            format = "{icon} {count}";
            show-empty = false;
            format-icons = [ "" "" ];
            tooltip = true;
            tooltip-format = "{app}: {title}";
          };
          "custom/notification" = {
            tooltip = false;
            format = "{} {icon}";
            format-icons = {
              notification = "<span foreground='red'><sup></sup></span>";
              none = "";
              dnd-notification = "<span foreground='red'><sup></sup></span>";
              dnd-none = "";
              inhibited-notification = "<span foreground='red'><sup></sup></span>";
              inhibited-none = "";
              dnd-inhibited-notification = "<span foreground='red'><sup></sup></span>";
              dnd-inhibited-none = "";
            };
            return-type = "json";
            exec = "${pkgs.swaynotificationcenter}/bin/swaync-client -swb";
            on-click = "${pkgs.swaynotificationcenter}/bin/swaync-client -t -sw";
            on-click-right = "${pkgs.swaynotificationcenter}/bin/swaync-client -d -sw";
            escape = true;
          };
          mpd = {
            format = "{stateIcon} {consumeIcon}{randomIcon}{repeatIcon}{singleIcon}{artist} - {album} - {title} ({elapsedTime:%M:%S}/{totalTime:%M:%S}) ⸨{songPosition}|{queueLength}⸩ {volume}% ";
            format-disconnected = "Disconnected ";
            format-stopped = "{consumeIcon}{randomIcon}{repeatIcon}{singleIcon}Stopped ";
            unknown-tag = "N/A";
            interval = 2;
            consume-icons.on = " ";
            random-icons = {
              off = "<span color=\"#f53c3c\"></span> ";
              on = " ";
            };
            repeat-icons.on = " ";
            single-icons.on = "1 ";
            state-icons = {
              paused = "";
              playing = "";
            };
            tooltip-format = "MPD (connected)";
            tooltip-format-disconnected = "MPD (disconnected)";
          };
          "idle_inhibitor" = {
            format = "{icon}";
            format-icons = {
              activated = "";
              deactivated = "";
            };
          };
          tray.spacing = 10;
          clock = {
            tooltip-format = "<big>{:%Y %B}</big>\n<tt><small>{calendar}</small></tt>";
            format-alt = "{:%Y-%m-%d}";
          };
          cpu = {
            format = "{usage}% ";
            tooltip = false;
            on-click = "${pkgs.${wm.terminal}}/bin/${wm.terminal} zenith"; # change to top variable to be passed from system
          };
          memory = {
            format = "{}% ";
            on-click = "${pkgs.${wm.terminal}}/bin/${wm.terminal} zenith"; # change to top variable to be passed from system
          };
          temperature = {
            critical-threshold = "80";
            format = "{temperatureC}°C {icon}";
            format-icons = [ "" ];
          };
          backlight = {
            format = "{percent}% {icon}";
            format-icons = [ "" "" "" "" "" "" "" "" "" ];
          };
          battery = {
            states = {
              warning = 30;
              critical = 15;
            };
            format = "{capacity}% {icon}";
            format-charging = "{capacity}% ";
            format-plugged = "{capacity}% ";
            format-alt = "{time} {icon}";
            format-icons = [ "" "" "" "" "" ];
          };
          network = {
            format-wifi = "{essid} ({signalStrength}%) ";
            format-ethernet = "{ipaddr}/{cidr} ";
            tooltip-format = "{ifname} via {gwaddr} ";
            format-linked = "{ifname} (No IP) ";
            format-disconnected = "Disconnected ⚠";
            format-alt = "{ifname}: {ipaddr}/{cidr}";
          };
          pulseaudio = {
            format = "{volume}% {icon} {format_source}";
            format-bluetooth = "{volume}% {icon} {format_source}";
            format-bluetooth-muted = "󰝟 {icon} {format_source}";
            format-muted = "󰝟 {format_source}";
            format-source = "{volume}% ";
            format-source-muted = "";
            format-icons = {
              headphone = "";
              hands-free = "";
              headset = "";
              phone = "";
              portable = "";
              car = "";
              default = [ "" "" "" ];
            };
            # does not run when clicked unless ran from terminal
            on-click = "${pkgs.pavucontrol}/bin/pavucontrol";
          };
          # styling isn't getting applied for whatever reason
          style = ''
            * {
              border: none;
              border-radius: 0;
              font-size: 20 px;
            }
          ''; #/etc/nixos/waybar.css;
        };
      };
    };
  };
}
