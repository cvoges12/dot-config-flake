{ lib, wm, msg, menu, ... }:

with lib;

(
  # change focus
  attrsets.mapAttrs'
    (name: value: attrsets.nameValuePair
      "${wm.mod}+${value}"
      "focus ${name}")
    { inherit (wm) left right up down; }
) // (
  # move
  attrsets.mapAttrs'
    (name: value: attrsets.nameValuePair
      "${wm.mod}+Shift+${value}"
      "move ${name}")
    { inherit (wm) left right up down; }
) // (
  # change workspace
  attrsets.mapAttrs'
    (name: value: attrsets.nameValuePair
      "${wm.mod}+${name}"
      "exec ${msg} fullscreen disable; mode default; exec ${msg} ${value}")
    { inherit (wm.ws) grave "1" "2" "3" "4" "5" "6" "7" "8" "9" "0"; }
) // (
  # move to workspace
  attrsets.mapAttrs'
    (name: value: attrsets.nameValuePair
      "${wm.mod}+Shift+${name}"
      "exec ${msg} fullscreen disable; exec ${msg} mode default; exec ${msg} move container to ${value}")
    { inherit (wm.ws) grave "1" "2" "3" "4" "5" "6" "7" "8" "9" "0"; }
) // {
  # change focus
  "${wm.mod}+p" = "focus parent";
  "${wm.mod}+c" = "focus child";

  # programs
  "${wm.mod}+space" = "exec --no-startup-id ${menu}";
  "${wm.mod}+Return" = "exec ${wm.terminal}";
  "${wm.mod}+Shift+Return" = "exec ${wm.terminal} --class floating-kitty";
  
  # environment
  "${wm.mod}+Shift+q" = "exec ${msg} exit";
  "${wm.mod}+w" = "kill";
  "${wm.mod}+button2" = "kill"; # FIX!
  #"${wm.mod}+b" = "exec ${msg} 'bar mode toggle'";

  # modes
  "${wm.mod}+Escape" = "exec ${msg} fullscreen disable; exec ${msg} mode default";
  "${wm.mod}+m" = "exec ${msg} fullscreen enable; exec ${msg} mode monocle";
  "${wm.mod}+r" = "exec ${msg} fullscreen disable; exec ${msg} mode resize";
  "${wm.mod}+f" = "focus mode_toggle";
  "${wm.mod}+Shift+f" = "floating toggle";

  # layout
  "${wm.mod}+s" = "layout stacking";
  "${wm.mod}+t" = "layout tabbed";
  "${wm.mod}+v" = "layout toggle split";
  "${wm.mod}+Shift+v" = "split toggle";
  
  # scratchpad
  "${wm.mod}+minus" = "exec ${msg} fullscreen disable; exec ${msg} mode default; exec ${msg} scratchpad show";
  "${wm.mod}+Shift+minus" = "exec ${msg} fullscreen disable; exec ${msg} mode default; exec ${msg} move scratchpad";
      
  # media controls
  "XF86AudioRaiseVolume" = "exec wpctl set-volume @DEFAULT_AUDIO_SINK@ 0.01+";
  "XF86AudioLowerVolume" = "exec wpctl set-volume @DEFAULT_AUDIO_SINK@ 0.01-";
  "XF86AudioMute" = "exec wpctl set-mute @DEFAULT_AUDIO_SINK@ toggle";
  "XF86AudioMicMute" = "exec wpctl set-mute @DEFAULT_AUDIO_SOURCE@ toggle";
  "XF86AudioPause" = "exec playerctl pause";
  "XF86AudioNext" = "exec playerctl next";
  "XF86AudioPrev" = "exec playerctl previous";
}
