{ config, pkgs, lib, ... }:

with lib;

let
  cfg = config.localModules.email;
in {
  options.localModules.email = {
    enable = mkEnableOption (mdDoc "email");
  };

  config = mkIf cfg.enable {
    home.packages = with pkgs; [ bitwarden-cli ];
    accounts.email.accounts = {
      vogesclaytonGmail = rec {
        primary = true;
        realName = "Clayton Voges";
        address = "vogesclayton@gmail.com";
        userName = address;
        passwordCommand = "{pkgs.bitwarden-cli}/bin/bw get password google.com";
  
        imap = {
          host = "imap.gmail.com";
          port = 993;
        };
        smtp = {
          host = "smtp.gmail.com";
          port = 465;
        };
  
        thunderbird = {
          enable = true;
          profiles = [ "default" ];
        };
      };
    };
  
    programs.thunderbird = {
      enable = true;
      profiles.default = {
        isDefault = true;
        settings = {
          "mail.spellcheck.inline" = false;
        };
        userContent = ''
          /* Hide scrollbar on Thunderbird pages */
          *{scrollbar-width:none !important}
        '';
      };
  
      settings = {
        "general.useragent.override" = "";
        "privacy.donottrackheader.enabled" = true;
      };
    };
  };
}
