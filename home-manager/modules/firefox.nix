{ config, lib, pkgs, ... }:

with lib;

let
  cfg = config.localModules.firefox;

  inherit (pkgs.stdenv.hostPlatform) isDarwin;
  ncfg = config.programs.firefox;

  wayfirefox = pkgs.firefox.overrideAttrs (old: {
    buildCommand = old.buildCommand +
      (if config.xsession.enable then
        ''
          mv $out/bin/firefox $out/bin/wayfirefox
          wrapProgram $out/bin/wayfirefox \
            --set MOZ_ENABLE_WAYLAND 1 \
            --set GDK_BACKEND "wayland" \
            --add-flags '--profile ${config.home.homeDirectory}/.mozilla/firefox/wayland -no-remote -new-instance'
        ''
      else
        ''
          wrapProgram $out/bin/firefox \
            --set MOZ_ENABLE_WAYLAND 1 \
            --set GDK_BACKEND "wayland" \
            --add-flags '--profile ${config.home.homeDirectory}/.mozilla/firefox/wayland -no-remote -new-instance'
        ''
      );
  });

  xfirefox = pkgs.firefox.overrideAttrs (old: {
    buildCommand = old.buildCommand +
      (if config.xsession.enable then
        ''
          wrapProgram $out/bin/firefox \
            --unset MOZ_ENABLE_WAYLAND \
            --unset GDK_BACKEND \
            --unset WAYLAND_DISPLAY \
            --add-flags '--profile ${config.home.homeDirectory}/.mozilla/firefox/xorg -no-remote -new-instance'
        ''
      else
        ''
          mv $out/bin/firefox $out/bin/xfirefox
          wrapProgram $out/bin/xfirefox \
            --unset MOZ_ENABLE_WAYLAND \
            --unset GDK_BACKEND \
            --unset WAYLAND_DISPLAY \
            --add-flags '--profile ${config.home.homeDirectory}/.mozilla/firefox/xorg -no-remote -new-instance'
        ''
      );
  });
in {
  options.localModules.firefox = {
    enable = mkEnableOption (mdDoc "firefox");
    addons.enable = mkEnableOption (mdDoc "add ons");
    settings.enable = mkEnableOption (mdDoc "settings");
    clearBrowser.enable = mkEnableOption (mdDoc "clears the browser");
  };

  config = mkIf cfg.enable {
    home = {
      # until firefox on wayland is faster
      sessionVariables.BROWSER = "xfirefox";

      # install xfirefox
      packages = let
        # The configuration expected by the Firefox wrapper.
        fcfg = { inherit (ncfg) enableGnomeExtensions; };
      in [
        (if isDarwin then
          xfirefox
        else if versionAtLeast config.home.stateVersion "19.09" then
          xfirefox.override (old: { cfg = old.cfg or {} // fcfg; })
        else
          (pkgs.wrapFirefox.override {
            config = setAttrByPath [(
              ncfg.package.browserName
                or (builtins.parseDrvName
                  ncfg.package.name
                ).name
            )] fcfg;
          }) xfirefox {}
        )
      ];
    };

    programs.firefox = {
      enable = true;
      package = if config.xsession.enable then
        xfirefox
      else
        wayfirefox;

      profiles = let
        # https://superuser.com/questions/1669675/enable-all-firefox-extensions-in-private-mode-by-default
        #
        # run this in browser console (ctrl+shift+j):
        #
        #(async () => {
        #    for(const addon of await AddonManager.getAddonsByTypes(["extension"])){
        #        let policy = WebExtensionPolicy.getByID(addon.id);
        #
        #        await ChromeUtils
        #            .import("resource://gre/modules/ExtensionPermissions.jsm")["ExtensionPermissions"]
        #            .add(addon.id, {
        #                permissions: ["internal:privateBrowsingAllowed"],
        #                origins: [],
        #            }, policy && policy.extension);
        #
        #        addon.reload();
        #    }
        #})();
        extensions = mkIf
          cfg.addons.enable
          (with pkgs.nur.repos.rycee.firefox-addons; [
            # requested extensions:
            #kubernetes-search-extension
            #svelte-devtools
            #svelte-detector

            #google-lighthouse
            # or
            #lighthouse-report-generator

            terms-of-service-didnt-read
            link-cleaner

            # site limits #
            # remove anti- pop up
            limit-limit-distracting-sites

            # passwords #
            bitwarden

            # ads #
            ublock-origin

            # writing / language #
            languagetool
            firefox-translations

            # financial #
            honey
            pay-by-privacy-com

            # youtube #
            return-youtube-dislikes
            sponsorblock
            youtube-shorts-block
            peertubeify

            # archival #
            unpaywall
            #web-archives

            # need to actually configure one of these #
            #ghosttext # requires editor plugin
            # or
            #textern # requires native app
            # or
            #firenvim

            libredirect
            # or
            #privacy-redirect

            # fix #
            #wayback-machine

            # search #
            image-search-options  # right click image
            search-by-image # access through add-on
            js-search-extension # js
            rust-search-extension # rs
            c-c-search-extension # cc
            
            # programming tools #
            a11ycss
            react-devtools

            # Won't use #

            # No blank URL bar; just use bookmarks
            #new-tab-override

            # using dracula or GTK theme instead
            #tokyo-night-v2

            # (I think) causing slow downs
            #video-downloadhelper
            #youtube-recommended-videos # clunky
          ]);

        search = {
          default = "Qwant";
          force = true;
          engines = {

            # General Purpose #
            "Qwant" = {
              urls = [{ template = "https://www.qwant.com/?q={searchTerms}"; }];
              definedAliases = [ "q" ];
              iconUpdateURL = "https://www.qwant.com/public/favicon-196.2bb406046d029085e6c528ddc4af2b3d.png";
              updateInterval = 24 * 60 * 60 * 1000;
            };
            "Startpage" = {
              urls = [{ template = "https://www.startpage.com/sp/search?q={searchTerms}"; }];
              definedAliases = [ "st" ];
              iconUpdateURL = "https://www.startpage.com/sp/cdn/favicons/apple-touch-icon-152x152--default.png";
              updateInterval = 24 * 60 * 60 * 1000;
            };
            "Google".metaData.hidden = true;
            "DuckDuckGo".metaData.hidden = true;
            "Bing".metaData.hidden = true;

            # Web Archives #
            "Archive Today" = {
              urls = [{ template = "https://archive.is/{searchTerms}"; }];
              definedAliases = [ "at" "archive-today" ];
              iconUpdateURL = "https://archive.is/apple-touch-icon.png";
              updateInterval = 24 * 60 * 60 * 1000;
            };
            "Wayback" = {
              urls = [{ template = "http://web.archive.org/web/20230000000000*/{searchTerms}"; }];
              definedAliases = [ "wb" "wayback" ];
              iconUpdateURL = "http://web.archive.org/_static/images/archive.ico";
              updateInterval = 24 * 60 * 60 * 1000;
            };
            "Google Cache" = {
              urls = [{ template = "http://webcache.googleusercontent.com/search?q=cache:{searchTerms}"; }];
              definedAliases = [ "gc" "google-cache" ];
              iconUpdateURL = "https://www.google.com/images/branding/googleg/1x/googleg_standard_color_128dp.png";
              updateInterval = 24 * 60 * 60 * 1000;
            };

            # Wikipedia #
            "Wikipedia (en)".metaData.alias = "w";
            "Wikipedia (de)" = {
              urls = [{ template = "https://de.wikipedia.org/wiki/{searchTerms}"; }];
              definedAliases = [ "wde" ];
              iconUpdateURL = "https://www.wikipedia.org/static/favicon/wikipedia.ico";
              updateInterval = 24 * 60 * 60 * 1000;
            };
            "Wikipedia (fr)" = {
              urls = [{ template = "https://fr.wikipedia.org/wiki/{searchTerms}"; }];
              definedAliases = [ "wfr" ];
              iconUpdateURL = "https://www.wikipedia.org/static/favicon/wikipedia.ico";
              updateInterval = 24 * 60 * 60 * 1000;
            };
            "Wikipedia (ru)" = {
              urls = [{ template = "https://ru.wikipedia.org/wiki/{searchTerms}"; }];
              definedAliases = [ "wru" ];
              iconUpdateURL = "https://www.wikipedia.org/static/favicon/wikipedia.ico";
              updateInterval = 24 * 60 * 60 * 1000;
            };
            "Wikipedia (es)" = {
              urls = [{ template = "https://es.wikipedia.org/wiki/{searchTerms}"; }];
              definedAliases = [ "wes" ];
              iconUpdateURL = "https://www.wikipedia.org/static/favicon/wikipedia.ico";
              updateInterval = 24 * 60 * 60 * 1000;
            };
            "Wikipedia (it)" = {
              urls = [{ template = "https://it.wikipedia.org/wiki/{searchTerms}"; }];
              definedAliases = [ "wit" ];
              iconUpdateURL = "https://www.wikipedia.org/static/favicon/wikipedia.ico";
              updateInterval = 24 * 60 * 60 * 1000;
            };
            "Wikipedia (ja)" = {
              urls = [{ template = "https://ja.wikipedia.org/wiki/{searchTerms}"; }];
              definedAliases = [ "wja" ];
              iconUpdateURL = "https://www.wikipedia.org/static/favicon/wikipedia.ico";
              updateInterval = 24 * 60 * 60 * 1000;
            };
            "Wikipedia (zh)" = {
              urls = [{ template = "https://zh.wikipedia.org/wiki/{searchTerms}"; }];
              definedAliases = [ "wzh" ];
              iconUpdateURL = "https://www.wikipedia.org/static/favicon/wikipedia.ico";
              updateInterval = 24 * 60 * 60 * 1000;
            };
            "Wikipedia (pt)" = {
              urls = [{ template = "https://pt.wikipedia.org/wiki/{searchTerms}"; }];
              definedAliases = [ "wpt" ];
              iconUpdateURL = "https://www.wikipedia.org/static/favicon/wikipedia.ico";
              updateInterval = 24 * 60 * 60 * 1000;
            };
            "Wikipedia (fa)" = {
              urls = [{ template = "https://fa.wikipedia.org/wiki/{searchTerms}"; }];
              definedAliases = [ "wfa" ];
              iconUpdateURL = "https://www.wikipedia.org/static/favicon/wikipedia.ico";
              updateInterval = 24 * 60 * 60 * 1000;
            };

            # Misc Tech #
            "Arch Linux Wiki" = {
              urls = [{ template = "https://wiki.archlinux.org/index.php?search={searchTerms}"; }];
              definedAliases = [ "arch" ];
              iconUpdateURL = "https://wiki.archlinux.org/favicon.ico";
              updateInterval = 24 * 60 * 60 * 1000;
            };
            #"MDN" = {
            #  urls = [{ template = "https://developer.mozilla.org/en-US/search?q={searchTerms}"; }];
            #  definedAliases = [ "mdn" "m" ];
            #  iconUpdateURL = "https://developer.mozilla.org/favicon-48x48.cbbd161b.png";
            #  updateInterval = 24 * 60 * 60 * 1000;
            #};

            # Source Code #
            "Phind" = {
              urls = [{ template = "https://www.phind.com/search?q={searchTerms}"; }];
              definedAliases = [ "p" ];
              iconUpdateURL = "https://www.phind.com/images/favicon.png";
              updateInterval = 24 * 60 * 60 * 1000;
            };
            "Sourcegraph" = {
              urls = [{ template = "https://sourcegraph.com/search?patternType=standard&sm=1&groupBy=repo&q=context%3Aglobal+{searchTerms}"; }];
              definedAliases = [ "src" "s" ];
              iconUpdateURL = "https://s2.googleusercontent.com/s2/favicons?domain=sourcegraph.com";
              updateInterval = 24 * 60 * 60 * 1000;
            };

            # Nix #
            "NixOS Wiki" = {
              urls = [{ template = "https://nixos.wiki/index.php?search={searchTerms}"; }];
              definedAliases = [ "nw" ];
              iconUpdateURL = "https://nixos.wiki/favicon.png";
              updateInterval = 24 * 60 * 60 * 1000;
            };
            "Nix Packages" = {
              urls = [{
                template = "https://search.nixos.org/packages";
                params = [
                  { name = "type"; value = "packages"; }
                  { name = "query"; value = "{searchTerms}"; }
                ];
              }];
              definedAliases = [ "np" ];
              iconUpdateURL = "https://nixos.org/favicon.png";
              updateInterval = 24 * 60 * 60 * 1000;
            };
            "Nix Options" = {
              urls = [{
                template = "https://search.nixos.org/options";
                params = [
                  { name = "type"; value = "options"; }
                  { name = "query"; value = "{searchTerms}"; }
                ];
              }];
              definedAliases = [ "no" ];
              iconUpdateURL = "https://nixos.org/favicon.png";
              updateInterval = 24 * 60 * 60 * 1000;
            };
            "Noogle" = {
              urls = [{ template = "https://noogle.dev/?itemsPerPage=100&term=\"{searchTerms}\""; }]; 
              definedAliases = [ "ng" ];
              iconUpdateURL = "https://nixos.org/favicon.png";
              updateInterval = 24 * 60 * 60 * 1000;
            };

            # Misc #
            "YouTube" = {
              urls = [{ template = "https://www.youtube.com/results?search_query={searchTerms}"; }];
              definedAliases = [ "y" "yt" ];
              iconUpdateURL = "https://www.youtube.com/s/desktop/932eb6a8/img/favicon_144x144.png";
              updateInterval = 24 * 60 * 60 * 1000;
            };
            "Google Images" = {
              urls = [{ template = "https://www.google.com/search?tbm=isch&q={searchTerms}"; }];
              definedAliases = [ "gi" ];
              iconUpdateURL = "https://www.google.com/images/branding/googleg/1x/googleg_standard_color_128dp.png";
              updateInterval = 24 * 60 * 60 * 1000;
            };
            "Google Maps" = {
              urls = [{ template = "https://www.google.com/maps/search/{searchTerms}"; }];
              definedAliases = [ "gm" "maps" ];
              iconUpdateURL = "https://www.google.com/images/branding/product/ico/maps15_bnuw3a_32dp.ico";
              updateInterval = 24 * 60 * 60 * 1000;
            };
            "Google Translate" = {
              urls = [{ template = "https://translate.google.com/?sl=auto&tl=en&op=translate&text={searchTerms}"; }];
              definedAliases = [ "gt" "trans" ];
              iconUpdateURL = "https://www.google.com/images/branding/product/ico/maps15_bnuw3a_32dp.ico";
              updateInterval = 24 * 60 * 60 * 1000;
            };
            "Amazon.com".metaData.alias = "a";
            "eBay".metaData.alias = "e";
            "Reddit" = {
              urls = [{ template = "https://www.reddit.com/search/?q={searchTerms}"; }];
              definedAliases = [ "r" "red" ];
              iconUpdateURL = "https://www.redditstatic.com/shreddit/assets/favicon/192x192.png";
              updateInterval = 24 * 60 * 60 * 1000;
            };
          };
          order = [
            "Bookmarks"
            "Tabs"
            "History"
            "Qwant"
            "Startpage"
            "Archive Today"
            "Wayback"
            "Google Cache"
            "Wikipedia (en)"
            "Arch Linux Wiki"
            #"MDN"
            "Phind"
            "Sourcegraph"
            "Nix Packages"
            "Nix Options"
            "Noogle"
            "NixOS Wiki"
          ];
        };

        bookmarks = [
          {
            name = "Bookmark Toolbar";
            toolbar = true;
            bookmarks = [
              {
                name = "Productivity";
                bookmarks = [
                  {
                    name = "Trello";
                    keyword = "trello";
                    url = "https://trello.com";
                  }
                  {
                    name = "NextCloud";
                    keyword = "nextcloud";
                    url = "https://cvoges12.thegood.cloud";
                  }
                  {
                    name = "Jira";
                    keyword = "jira";
                    url = "https://start.atlassian.com";
                  }
                ];
              }
              {
                name = "Social";
                bookmarks = [
                  {
                    name = "Gmail";
                    keyword = "google";
                    url = "https://gmail.com";
                  }
                  {
                    name = "Matrix";
                    keyword = "matrix";
                    url = "https://chat.tchncs.de";
                  }
                  {
                    name = "Discord";
                    keyword = "discord";
                    url = "https://discord.com/app";
                  }
                ];
              }
              {
                name = "Dev";
                bookmarks = [
                  {
                    name = "Nix";
                    bookmarks = [
                      {
                        name = "Home-manager";
                        bookmarks = [
                          {
                            name = "Home-manager Options: Appendix A";
                            url = "https://nix-community.github.io/home-manager/options.html";
                            keyword = "home";
                          }
                        ];
                      }
                      {
                        name = "Nix Manual";
                        url = "https://nixos.org/manual/nix";
                      }
                      {
                        name = "NixOS Manual";
                        url = "https://nixos.org/manual/nixos";
                      }
                      {
                        name = "NixOS Guide: Nix Lang";
                        url = "https://nixos.org/guides/nix-language.html";
                      }
                      {
                        name = "NixOS Guide: Nix Pills";
                        url = "https://nixos.org/guides/nix-pills";
                      }
                      {
                        name = "NixOS Wiki: Nix Lang";
                        url = "https://nixos.wiki/wiki/Overview_of_the_Nix_Language";
                      }
                      {
                        name = "Github: NixOS Guide";
                        url = "https://github.com/mikeroyal/NixOS-Guide";
                      }
                      {
                        name = "NixOS Config Collection";
                        url = "https://nixos.wiki/wiki/Configuration_Collection";
                      }
                      {
                        name = "Development Environment with nix-shell";
                        url = "https://nixos.wiki/wiki/Development_environment_with_nix-shell";
                      }
                      {
                        name = "Nix shell package development";
                        url = "https://nixos.wiki/wiki/Nixpkgs/Create_and_debug_packages#Using_nix-shell_for_packages_development";
                      }
                      {
                        name = "Packaging Binaries";
                        url = "https://nixos.wiki/wiki/Packaging/Binaries";
                      }
                      {
                        name = "Packaging Examples";
                        url = "https://nixos.wiki/wiki/Packaging/Examples";
                      }
                      {
                        name = "Stack Exchange Package Derivation";
                        url = "https://unix.stackexchange.com/questions/717168/how-to-package-my-software-in-nix-or-write-my-own-package-derivation-for-nixpkgs";
                      }
                      {
                        name = "Nix Python";
                        bookmarks = [
                          {
                            name = "Github: Python on Nix";
                            url = "https://github.com/FRidh/python-on-nix/blob/master/tutorial.md";
                          }
                          {
                            name = "Packaging Python";
                            url = "https://nixos.wiki/wiki/Packaging/Python";
                          }
                        ];
                      }
                    ];
                  }
                  {
                    name = "Frontend";
                    bookmarks = [
                      {
                        name = "Svelte";
                        bookmarks = [
                          {
                            name = "Svelte Tutorial";
                            url = "https://svelte.dev/tutorial/event-modifiers";
                          }
                        ];
                      }
                    ];
                  }
                  {
                    name = "Git";
                    bookmarks = [
                      {
                      name = "Learn Git";
                      url = "https://learngitbranching.js.org";
                    }
                      {
                      name = "Git Tutorials";
                      url = "https://atlassian.com/git/tutorials";
                    }
                    ];
                  }
                ];
              }
              {
                name = "LAN";
                bookmarks = [
                  {
                    name = "cvoges12-bed-linksys-openwrt";
                    url = "http://cvoges12-bed-linksys-openwrt.cvoges12-stl0.lan";
                  }
                  {
                    name = "cvoges12-a0-24-opnsense";
                    url = "http://cvoges12-a0-24-opnsense.cvoges12-stl0.lan";
                  }
                ];
              }
            ];
          }
          {
            name = "Firefox";
            bookmarks = [
              {
                name = "Firefor Profile";
                url = "https://ffprofile.com";
              }
              {
                name = "Firefox Config";
                url = "about:config";
              }
              {
                name = "Firefox Enable all extensions";
                url = "https://superuser.com/questions/1669675/enable-all-firefox-extensions-in-private-mode-by-default";
              }
            ];
          }
          {
            name = "Mordhau Guide";
            url = "https://steamcommunity.com/sharedfiles/filedetails/?id=1640297858";
          }
        ];

        settings = attrsets.optionalAttrs cfg.settings.enable {
          # still have to manually select theme

          "browser.compactmode.show" = true;
          #"browser.startup.homepage" = "https://trello.com/";
          "browser.tabs.inTitlebar" = 1;
          "browser.toolbars.bookmarks.visibility" = "newtab";
          #"browser.theme.toolbar-theme" = 0;
          
          "browser.uiCustomization.state" = "{\"placements\":{\"widget-overflow-fixed-list\":[],\"unified-extensions-area\":[\"a11y_css_ffoodd-browser-action\",\"_446900e4-71c2-419f-a6a7-df9c091e268b_-browser-action\",\"_e737d9cb-82de-4f23-83c6-76f70a82229c_-browser-action\",\"ghosttext_bfred_it-browser-action\",\"_479ec4ee-fd16-4f95-b172-dd39fbd921ad_-browser-action\",\"7esoorv3_alefvanoon_anonaddy_me-browser-action\",\"_26ebede3-10ce-443c-bb0e-7f490cad0ec8_-browser-action\",\"privacy_privacy_com-browser-action\",\"_react-devtools-browser-action\",\"_762f9885-5a13-4abd-9c77-433dcd38b8fd_-browser-action\",\"_04188724-64d3-497b-a4fd-7caffe6eab29_-browser-action\",\"_2e5ff8c8-32fe-46d0-9fc8-6b8986621f3c_-browser-action\",\"sourcegraph-for-firefox_sourcegraph_com-browser-action\",\"sponsorblocker_ajay_app-browser-action\",\"ublock0_raymondhill_net-browser-action\",\"myallychou_gmail_com-browser-action\",\"_f209234a-76f0-4735-9920-eb62507a54cd_-browser-action\",\"_b9db16a4-6edc-47ec-a1f4-b86292ed211d_-browser-action\",\"wayback_machine_mozilla_org-browser-action\",\"_bc9bb30a-c6a3-4404-b116-574f1208e70b_-browser-action\",\"_34daeb50-c2d2-4f14-886a-7160b24d66a4_-browser-action\"],\"nav-bar\":[\"back-button\",\"forward-button\",\"stop-reload-button\",\"urlbar-container\",\"save-to-pocket-button\",\"downloads-button\"],\"toolbar-menubar\":[\"menubar-items\"],\"TabsToolbar\":[\"firefox-view-button\",\"tabbrowser-tabs\",\"alltabs-button\"],\"PersonalToolbar\":[\"bookmarks-menu-button\",\"personal-bookmarks\"]},\"seen\":[\"developer-button\",\"a11y_css_ffoodd-browser-action\",\"_446900e4-71c2-419f-a6a7-df9c091e268b_-browser-action\",\"_e737d9cb-82de-4f23-83c6-76f70a82229c_-browser-action\",\"ghosttext_bfred_it-browser-action\",\"_479ec4ee-fd16-4f95-b172-dd39fbd921ad_-browser-action\",\"7esoorv3_alefvanoon_anonaddy_me-browser-action\",\"_26ebede3-10ce-443c-bb0e-7f490cad0ec8_-browser-action\",\"privacy_privacy_com-browser-action\",\"_react-devtools-browser-action\",\"_762f9885-5a13-4abd-9c77-433dcd38b8fd_-browser-action\",\"_04188724-64d3-497b-a4fd-7caffe6eab29_-browser-action\",\"_2e5ff8c8-32fe-46d0-9fc8-6b8986621f3c_-browser-action\",\"sourcegraph-for-firefox_sourcegraph_com-browser-action\",\"sponsorblocker_ajay_app-browser-action\",\"ublock0_raymondhill_net-browser-action\",\"myallychou_gmail_com-browser-action\",\"_f209234a-76f0-4735-9920-eb62507a54cd_-browser-action\",\"_b9db16a4-6edc-47ec-a1f4-b86292ed211d_-browser-action\",\"wayback_machine_mozilla_org-browser-action\",\"_bc9bb30a-c6a3-4404-b116-574f1208e70b_-browser-action\",\"_34daeb50-c2d2-4f14-886a-7160b24d66a4_-browser-action\"],\"dirtyAreaCache\":[\"nav-bar\",\"unified-extensions-area\",\"toolbar-menubar\",\"TabsToolbar\",\"PersonalToolbar\"],\"currentVersion\":19,\"newElementCount\":4}";
          "browser.uidensity" = 1;
          "devtools.chrome.enabled" = true;
          #"extensions.pictureinpicture.enable_picture_in_picture_overrides" = true;

          # https://ffprofile.com/#form0
          "app.normandy.api_url" = "";
          "app.normandy.enabled" = false;
          "app.shield.optoutstudies.enabled" = false;
          "app.update.auto" = false;
          "beacon.enabled" = false;
          "breakpad.reportURL" = "";
          "browser.aboutConfig.showWarning" = false;
          "browser.cache.offline.enable" = false;
          "browser.crashReports.unsubmittedCheck.autoSubmit" = false;
          "browser.crashReports.unsubmittedCheck.autoSubmit2" = false;
          "browser.crashReports.unsubmittedCheck.enabled" = false;
          "browser.disableResetPrompt" = true;
          "browser.newtab.preload" = false;
          "browser.newtabpage.activity-stream.section.highlights.includePocket" = false;
          "browser.newtabpage.enabled" = false;
          "browser.newtabpage.enhanced" = false;
          "browser.newtabpage.introShown" = true;
          "browser.safebrowsing.appRepURL" = "";
          "browser.safebrowsing.blockedURIs.enabled" = false;
          "browser.safebrowsing.downloads.enabled" = false;
          "browser.safebrowsing.downloads.remote.enabled" = false;
          "browser.safebrowsing.downloads.remote.url" = "";
          "browser.safebrowsing.enabled" = false;
          "browser.safebrowsing.malware.enabled" = false;
          "browser.safebrowsing.phishing.enabled" = false;
          "browser.selfsupport.url" = "";
          "browser.send_pings" = false;
          "browser.sessionstore.privacy_level" = 0;
          "browser.shell.checkDefaultBrowser" = false;
          "browser.startup.homepage_override.mstone" = "ignore";
          "browser.tabs.crashReporting.sendReport" = false;
          "browser.tabs.firefox-view" = false;
          "browser.urlbar.groupLabels.enabled" = false;
          "browser.urlbar.quicksuggest.enabled" = false;
          "browser.urlbar.trimURLs" = false;
          "datareporting.healthreport.service.enabled" = false;
          "datareporting.healthreport.uploadEnabled" = false;
          "datareporting.policy.dataSubmissionEnabled" = false;
          "device.sensors.ambientLight.enabled" = false;
          "device.sensors.enabled" = false;
          "device.sensors.motion.enabled" = false;
          "device.sensors.orientation.enabled" = false;
          "device.sensors.proximity.enabled" = false;
          "dom.battery.enabled" = false;
          "experiments.activeExperiment" = false;
          "experiments.enabled" = false;
          "experiments.manifest.uri" = "";
          "experiments.supported" = false;
          "extensions.blocklist.enabled" = false;
          "extensions.getAddons.cache.enabled" = false;
          "extensions.getAddons.showPane" = false;
          "extensions.greasemonkey.stats.optedin" = false;
          "extensions.greasemonkey.stats.url" = "";
          "extensions.pocket.enabled" = false;
          "extensions.shield-recipe-client.api_url" = "";
          "extensions.shield-recipe-client.enabled" = false;
          "extensions.webservice.discoverURL" = "";
          "media.autoplay.default" = 2;
          "media.autoplay.enabled" = false;
          "media.eme.enabled" = false;
          "media.gmp-widevinecdm.enabled" = false;
          "media.navigator.enabled" = false;
          "media.video_stats.enabled" = false;
          "network.IDN_show_punycode" = true;
          "network.allow-experiments" = false;
          "network.captive-portal-service.enabled" = false;
          "network.cookie.cookieBehavior" = 1;
          "network.dns.disablePrefetch" = true;
          "network.dns.disablePrefetchFromHTTPS" = true;
          "network.http.referer.spoofSource" = true;
          "network.predictor.enable-prefetch" = false;
          "network.predictor.enabled" = false;
          "network.prefetch-next" = false;
          "network.trr.mode" = 5;
          "privacy.donottrackheader.enabled" = true;
          "privacy.donottrackheader.value" = 1;
          "privacy.query_stripping" = true;
          "privacy.trackingprotection.cryptomining.enabled" = true;
          "privacy.trackingprotection.enabled" = true;
          "privacy.trackingprotection.fingerprinting.enabled" = true;
          "privacy.trackingprotection.pbmode.enabled" = true;
          "privacy.usercontext.about_newtab_segregation.enabled" = true;
          "security.ssl.disable_session_identifiers" = true;
          "services.sync.prefs.sync.browser.newtabpage.activity-stream.showSponsoredTopSite" = false;
          "signon.autofillForms" = false;
          "toolkit.telemetry.archive.enabled" = false;
          "toolkit.telemetry.bhrPing.enabled" = false;
          "toolkit.telemetry.cachedClientID" = "";
          "toolkit.telemetry.enabled" = false;
          "toolkit.telemetry.firstShutdownPing.enabled" = false;
          "toolkit.telemetry.hybridContent.enabled" = false;
          "toolkit.telemetry.newProfilePing.enabled" = false;
          "toolkit.telemetry.prompted" = 2;
          "toolkit.telemetry.rejected" = true;
          "toolkit.telemetry.reportingpolicy.firstRun" = false;
          "toolkit.telemetry.server" = "";
          "toolkit.telemetry.shutdownPingSender.enabled" = false;
          "toolkit.telemetry.unified" = false;
          "toolkit.telemetry.unifiedIsOptIn" = false;
          "toolkit.telemetry.updatePing.enabled" = false;
          "webgl.renderer-string-override" = " ";
          "webgl.vendor-string-override" = " ";
        #} // attrsets.optionalAttrs config.localModules.wm.sway.enable {
        #  # https://bbs.archlinux.org/viewtopic.php?id=281398
        #  # https://ubuntuhandbook.org/index.php/2021/08/enable-hardware-video-acceleration-va-api-for-firefox-in-ubuntu-20-04-18-04-higher/
        #  "media.ffmpeg.vaapi.enabled" = true;
        #  "media.ffvpx.enabled" = false;
        #  "media.rdd-vpx.enabled" = false;
        #  "media.navigator.mediadatadecoder_vpx_enabled" = true;
        #  "media.hardware-video-decoding.force-enabled" = true;
        } // attrsets.optionalAttrs cfg.addons.enable {
          #"extensions.webextensions.ExtensionStorageIDB.migrated.7esoorv3@alefvanoon.anonaddy.me" = true;
          #"extensions.webextensions.ExtensionStorageIDB.migrated.a11y.css@ffoodd" = true;
          #"extensions.webextensions.ExtensionStorageIDB.migrated.ghosttext@bfred.it" = true;
          #"extensions.webextensions.ExtensionStorageIDB.migrated.jid0-3GUEt1r69sQNSrca5p8kx9Ezc3U@jetpack" = true;
          #"extensions.webextensions.ExtensionStorageIDB.migrated.screenshots@mozilla.org" = true;
          #"extensions.webextensions.ExtensionStorageIDB.migrated.sponsorBlocker@ajay.app" = true;
          #"extensions.webextensions.ExtensionStorageIDB.migrated.uBlock0@raymondhill.net" = true;
          #"extensions.webextensions.ExtensionStorageIDB.migrated.wayback_machine@mozilla.org" = true;
          #"extensions.webextensions.ExtensionStorageIDB.migrated.{04188724-64d3-497b-a4fd-7caffe6eab29}" = true;
          #"extensions.webextensions.ExtensionStorageIDB.migrated.{26ebede3-10ce-443c-bb0e-7f490cad0ec8}" = true;
          #"extensions.webextensions.ExtensionStorageIDB.migrated.{2e5ff8c8-32fe-46d0-9fc8-6b8986621f3c}" = true;
          #"extensions.webextensions.ExtensionStorageIDB.migrated.{446900e4-71c2-419f-a6a7-df9c091e268b}" = true;
          #"extensions.webextensions.ExtensionStorageIDB.migrated.{4a313247-8330-4a81-948e-b79936516f78}" = true;
          #"extensions.webextensions.ExtensionStorageIDB.migrated.{b9db16a4-6edc-47ec-a1f4-b86292ed211d}" = true;
          #"extensions.webextensions.ExtensionStorageIDB.migrated.{e737d9cb-82de-4f23-83c6-76f70a82229c}" = true;
          #"extensions.webextensions.ExtensionStorageIDB.migrated.{f209234a-76f0-4735-9920-eb62507a54cd}" = true;
          #"extensions.activeThemeID" = "{afda92c3-008d-4d08-8766-3f1571995071}";
        } // attrsets.optionalAttrs cfg.clearBrowser.enable {
          "privacy.clearOnShutdown.cache" = true;
          "privacy.clearOnShutdown.cookies" = true;
          "privacy.clearOnShutdown.downloads" = true;
          "privacy.clearOnShutdown.formdata" = true;
          "privacy.clearOnShutdown.history" = true;
          "privacy.clearOnShutdown.offlineApps" = false;
          "privacy.clearOnShutdown.openWindows" = false;
          "privacy.clearOnShutdown.sessions" = true;
          "privacy.clearOnShutdown.siteSettings" = true;
          "privacy.history.custom" = true;
          "privacy.purge_trackers.date_in_cookie_database" = 0;
          "privacy.sanitize.pending" = "[{\"id\":\"shutdown\",\"itemsToClear\":[\"cache\",\"cookies\",\"history\",\"formdata\",\"downloads\",\"sessions\",\"siteSettings\"],\"options\":{}}]";
          "privacy.sanitize.sanitizeOnShutdown" = true;
          "signon.generation.enabled" = false;
          "signon.management.page.breach-alerts.enabled" = false;
          "signon.rememberSignons" = false;
        };
      in {
        wayland = {
          isDefault = !config.xsession.enable;
          id = 0;
          
          inherit extensions;
          inherit search;
          inherit bookmarks;
          inherit settings;
        };
        xorg = {
          isDefault = config.xsession.enable;
          id = 1;
          
          inherit extensions;
          inherit search;
          inherit bookmarks;
          inherit settings;
        };
      };
    };
  };
}
