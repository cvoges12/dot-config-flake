{ config, pkgs, lib, ... }:

with lib;

let
  cfg = config.localModules.shell;
in {
  imports = [
    ./fish.nix
  ];

  options.localModules.shell = {
    enable = mkEnableOption (mdDoc "shell");
    aliases = mkOption {
      type = with types; attrsOf str;
      default = {};
    };
  };

  config = mkIf cfg.enable {
    home = {
      sessionVariables = {
        FZF_DEFAULT_COMMAND =
          "fd --hidden --follow --exclude '.git' --exclude 'node_modules'"; #"rg --hidden --no-ignore --files";
        FZF_CTRL_T_COMMAND =
          config.home.sessionVariables.FZF_DEFAULT_COMMAND;
        FZF_ALT_C_COMMAND =
          "${config.home.sessionVariables.FZF_DEFAULT_COMMAND} --type d";
        FZF_DEFAULT_OPTS =
          "--layout=reverse --height=80% --info=inline --multi --preview-window=:hidden --preview 'if test -f {}; bat --style=numbers --color=always {}; else if test -d {}; tree -C -L 2 {} | less; end; echo {} 2> /dev/null | head -200' --color='hl:148,hl+:154,pointer:032,marker:010,bg+:237,gutter:008' --prompt='~ ' --pointer='▶' --marker='✗' --bind '?:toggle-preview' --bind 'ctrl-a:select-all' --bind 'ctrl-y:execute-silent(echo {+} | pbcopy)' --bind 'ctrl-e:execute(echo {+} | xargs -o $EDITOR)' --bind 'ctrl-v:execute(code {+})'";
      };

      packages = with pkgs; [
        fzf
        fd
      ];
    };
  };
}
