{ config, pkgs, lib, ... }:

with lib;

let
  inherit (config.localModules) shell;
  cfg = config.localModules.shell.fish;
in {
  options.localModules.shell.fish = {
    enable = mkEnableOption (mdDoc "fish");

    functions = mkOption {
      type = with types; attrsOf str;
      default = {};
    };
  };

  config = mkIf cfg.enable {
    programs = {
      fish = {
        inherit (cfg) functions;

        enable = true;
        shellAbbrs = shell.aliases;
        shellInit = ''
          set fish_greeting ""
        '';
        plugins = [
          # su -c "chown cvoges12 /home/cvoges12/.local/share/z/data"
          # chgrp users /home/cvoges12/.local/share/z/data
          # b/c of error:
          # `mv: replace '/home/cvoges12/.local/share/z/data', overriding mode 0600 (rw-------)?`
          #{
          #  name = "z";
          #  src = pkgs.fetchFromGitHub {
          #    owner = "jethrokuan";
          #    repo = "z";
          #    rev = "85f863f20f24faf675827fb00f3a4e15c7838d76";
          #    sha256 = "+FUBM7CodtZrYKqU542fQD+ZDGrd2438trKM0tIESs0=";
          #  };
          #}
          {
            name = "dracula";
            src = pkgs.fetchFromGitHub {
              owner = "dracula";
              repo = "fish";
              rev = "269cd7d76d5104fdc2721db7b8848f6224bdf554";
              sha256 = "Hyq4EfSmWmxwCYhp3O8agr7VWFAflcUe8BUKh50fNfY=";
            };
          }
        ];
      };

      starship = {
        enable = true;
        enableFishIntegration = true;
        settings = {
          character = {
            success_symbol = "[➜](bold green)";
            error_symbol = "[✗](bold red)";
          };
        };
      };
    };
  };
}
