{ config, lib, ... }:

with lib;

let
  cfg = config.localModules.newsboat;
in {
  options.localModules.newsboat = {
    enable = mkEnableOption (mdDoc "newsboat");
  };

  config = mkIf cfg.enable {
    programs.newsboat = {
      enable = true;
      autoReload = true;
    };
  };
}
