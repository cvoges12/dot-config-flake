{ config, lib, pkgs, ... }:

with lib;

let
  cfg = config.localModules.obs;
in {
  options.localModules.obs = {
    enable = mkEnableOption (mdDoc "obs");
  };

  config = mkIf cfg.enable {
    # Research for performance:
    # https://obsproject.com/kb/hardware-encoding
    # https://wiki.ubuntu.com/IntelQuickSyncVideo
    programs.obs-studio = {
      enable = true;
      plugins = with pkgs.obs-studio-plugins; [
        obs-gstreamer
        obs-pipewire-audio-capture
        obs-vkcapture
        input-overlay
        looking-glass-obs # install looking-glass too
        obs-backgroundremoval
        obs-move-transition
        obs-multi-rtmp
      ] ++ lists.optional (!config.xsession.enable) wlrobs;
    };
  };
}
