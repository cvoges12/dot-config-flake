{ config, lib, ... }:

with lib;

let
  cfg = config.localModules.user;
in {
  options.localModules.user = {
    enable = mkEnableOption (mdDoc "home user");

    name = mkOption {
      type = types.str;
      default = "";
    };

    packages = mkOption {
      type = with types; listOf package;
      default = [];
    };

    wallpaper = mkOption {
      type = types.path;
      default = "";
    };
  };
  config = mkIf cfg.enable {
    home = {
      inherit (cfg) packages;

      username = cfg.name;
      homeDirectory = "/home/${cfg.name}";
      stateVersion = "23.05";


      file."${config.xdg.dataHome}/wallpaper.jpg".source = cfg.wallpaper;
  
      sessionVariables.GTK_THEME = "Generated";
    };

    programs.home-manager.enable = true;
  };
}
