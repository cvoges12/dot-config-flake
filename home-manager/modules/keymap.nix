{ config, lib, ... }:

with lib;

let
  inherit (config.localModules.wm) sway;
  cfg = config.localModules.keymap;
in {
  options.localModules.keymap = {
    enable = mkEnableOption (mdDoc "key map");
    variant = mkOption {
      type = types.str;
      default = "qwerty";
      example = "colemak";
    };
    layout = mkOption {
      type = types.str;
      default = "us";
      example = "cz";
    };
  };

  config =  mkIf (cfg.enable && cfg.variant == "qwerty") {
    wayland.windowManager.sway.config.input."*" = mkIf sway.enable {
      xkb_layout = cfg.layout;
      xkb_options = "altwin:swap_alt_win";
    };

    localModules.wm = {
      left = "h";
      down = "j";
      up = "k";
      right = "l";
    };
  } // mkIf (cfg.enable && cfg.variant == "colemak") {
    wayland.windowManager.sway.config.input."*" = mkIf sway.enable {
      xkb_layout = cfg.layout;
      xkb_variant = "colemak";
      xkb_options = "altwin:swap_alt_win";
    };

    localModules.wm = {
      left = "h";
      down = "n";
      up = "e";
      right = "i";
    };
  };
}
