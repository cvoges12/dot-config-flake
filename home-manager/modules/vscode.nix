{ config, pkgs, lib, ... }:

with lib;

let
  cfg = config.localModules.vscode;
in {
  options.localModules.vscode = {
    enable = mkEnableOption (mdDoc "vscode");
    ai = mkEnableOption (mdDoc "AI completion");
    c = mkEnableOption (mdDoc "C / C++ and lower level programming");
    coq =  mkEnableOption (mdDoc "Coq");
    csv =  mkEnableOption (mdDoc "CSV");
    docker =  mkEnableOption (mdDoc "Docker");
    git =  mkEnableOption (mdDoc "Git");
    graphql =  mkEnableOption (mdDoc "GraphQL");
    java =  mkEnableOption (mdDoc "Java");
    jupyter =  mkEnableOption (mdDoc "Jupyter");
    k8s =  mkEnableOption (mdDoc "Kubernetes");
    llvm =  mkEnableOption (mdDoc "LLVM IR");
    lua =  mkEnableOption (mdDoc "Lua");
    markup =  mkEnableOption (mdDoc "markup");
    nvim =  mkEnableOption (mdDoc "Neovim");
    openscad =  mkEnableOption (mdDoc "OpenSCAD");
    py =  mkEnableOption (mdDoc "Python");
    rust =  mkEnableOption (mdDoc "rust");
    riscV = mkEnableOption (mdDoc "RISC-V");
    sh = mkEnableOption (mdDoc "Shell");
    texmd = mkEnableOption (mdDoc "LaTeX and Markdown");
    web = mkEnableOption (mdDoc "front-end, JS/TS, HTML/CSS, etc");
  };

  config = mkIf cfg.enable {
    programs.vscode = {
      enable = true;
      enableExtensionUpdateCheck = false;
      enableUpdateCheck = false;
      userSettings = {
        "extensions.autoCheckUpdates" = false;
        "update.mode" = "none";

        "files.autoSave" = "off";

        "editor.insertSpaces" = "on";
        "editor.tabSize" = 4;
        "editor.lineNumbers" = "relative";
        "editor.minimap.enabled" = false;

        "[nix]"."editor.tabSize" = 2;
        "[yaml]"."editor.tabSize" = 4;
        "[dockercompose]"."editor.tabSize" = 4;

        "workbench.colorTheme" = "Dracula";
        "workbench.preferredDarkColorTheme" = "Dracula";

        "trailing-spaces.includeEmptyLines" = false;
        "window.zoomLevel" = -1;

        "tabnine.experimentalAutoImports" = true;

        "nixEnvSelector.nixFile" = "$\{workspaceRoot\}/shell.nix";
        
        #"" = "on";
        # figure out how to fix this
        #"window" = {
        #  "autoDetectColorScheme" = "on";
        #  "" = "on";
        #};
      };
      extensions = with pkgs.vscode-extensions; [
        ms-vsliveshare.vsliveshare
        ms-vscode.anycode
        lokalise.i18n-ally
        jock.svg
        ibm.output-colorizer
        ms-vscode-remote.remote-ssh

        # Theme and Look
        dracula-theme.theme-dracula
        vscode-icons-team.vscode-icons
        file-icons.file-icons
        shardulm94.trailing-spaces
        kamikillerto.vscode-colorize
        spywhere.guides

        # Nix
        bbenoist.nix
        #jnoortheen.nix-ide
        b4dm4n.vscode-nixpkgs-fmt
        arrterian.nix-env-selector
      ] ++ lists.optionals cfg.ai [
        tabnine.tabnine-vscode
        github.copilot
        genieai.chatgpt-vscode
        chris-hayes.chatgpt-reborn
      ] ++ lists.optionals cfg.c [
        llvm-vs-code-extensions.vscode-clangd
        ms-vscode.cpptools
        vadimcn.vscode-lldb
        xaver.clang-format
        twxs.cmake
        ms-vscode.makefile-tools
        ms-vscode.hexeditor
      ] ++ lists.optionals cfg.coq [
        maximedenes.vscoq
      ] ++ lists.optionals cfg.csv [
        mechatroner.rainbow-csv
        grapecity.gc-excelviewer
      ] ++ lists.optionals cfg.docker [
        ms-azuretools.vscode-docker
      ] ++ lists.optionals cfg.git [
        waderyan.gitblame
        mhutchie.git-graph
        donjayamanne.githistory
        #eamodio.gitlens
        gitlab.gitlab-workflow
        github.vscode-pull-request-github
        github.codespaces
      ] ++ lists.optionals cfg.graphql [
        graphql.vscode-graphql
        graphql.vscode-graphql-syntax
        apollographql.vscode-apollo
      ] ++ lists.optionals cfg.java [
        redhat.java
        vscjava.vscode-java-debug
        vscjava.vscode-java-test
        vscjava.vscode-java-dependency
        vscjava.vscode-gradle
      ] ++ lists.optionals cfg.jupyter [
        ms-toolsai.jupyter
      ] ++ lists.optionals cfg.k8s [
        ms-kubernetes-tools.vscode-kubernetes-tools
      ] ++ lists.optionals cfg.markup [
        redhat.vscode-xml
        redhat.vscode-yaml
        jebbs.plantuml
        tamasfe.even-better-toml
      ] ++ lists.optionals cfg.llvm [
        colejcummins.llvm-syntax-highlighting
      ] ++ lists.optionals cfg.lua [
        sumneko.lua
      ] ++ lists.optionals cfg.nvim [
        asvetliakov.vscode-neovim
      ] ++ lists.optionals cfg.openscad [
        antyos.openscad
      ] ++ lists.optionals cfg.py [
        ms-python.python
        ms-python.vscode-pylance
        matangover.mypy
      ] ++ lists.optionals cfg.riscV [
        zhwu95.riscv
      ] ++ lists.optionals cfg.rust [
        rust-lang.rust-analyzer
        serayuzgur.crates
      ] ++ lists.optionals cfg.sh [
        timonwong.shellcheck
        skyapps.fish-vscode
        foxundermoon.shell-format
        mads-hartmann.bash-ide-vscode
      ] ++ lists.optionals cfg.texmd [
        valentjn.vscode-ltex
        james-yu.latex-workshop
        yzhang.markdown-all-in-one
        shd101wyy.markdown-preview-enhanced
        svsool.markdown-memo
        davidanson.vscode-markdownlint
        bierner.markdown-emoji
        bierner.markdown-checkbox
        bierner.emojisense
      ] ++ lists.optionals cfg.web [
        dbaeumer.vscode-eslint
        esbenp.prettier-vscode
        wix.vscode-import-cost
        svelte.svelte-vscode
        bradlc.vscode-tailwindcss
        firefox-devtools.vscode-firefox-debug
        humao.rest-client
      ];
    };
  };
}
