{ config, lib, ... }:

with lib;

let
  cfg = config.localModules.mpd;
in {
  options.localModules.mpd = {
    enable = mkEnableOption (mdDoc "mpd");
  };

  config = mkIf cfg.enable {
    services.mpd = {
      enable = true;
    };
  };
}
