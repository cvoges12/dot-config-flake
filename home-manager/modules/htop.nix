{ config, lib, ... }:

with lib;

let
  cfg = config.localModules.htop;
in {
  options.localModules.htop = {
    enable = mkEnableOption (mdDoc "htop");
  };

  config = mkIf cfg.enable {
    programs.htop = {
      enable = true;
      settings = {
        cpu_count_from_zero = 1;
        delay = 15;
        detailed_cpu_time = 1;
        fields = with config.lib.htop.fields; [
          PID
          USER
          PRIORITY
          NICE
          M_SIZE
          M_RESIDENT
          M_SHARE
          STATE
          PERCENT_CPU
          PERCENT_MEM
          TIME
          COMM
        ];
        header_margin = 1;
        highlight_base_name = 1;
        hightlight_megabytes = 1;
        hightlight_threads = 1;
        shadow_other_users = 1;
        show_program_path = 1;
        show_thread_names = 1;
        sort_direction = 1;
        sort_key = config.lib.htop.fields."PERCENT_MEM";
        tree_view = 0;
        update_process_names = 1;
      } // (with config.lib.htop; leftMeters [
        (bar "AllCPUs")
        (text "LoadAverage")
        (text "Uptime")
      ]) // (with config.lib.htop; rightMeters [
        (bar "Memory")
        (bar "Swap")
        (text "Zram")
        (text "Tasks")
        (text "Systemd")
      ]);
    };
  };
}
