{ config, lib, ... }:

with lib;

let
  cfg = config.localModules.bitwarden;
in {
  options.localModules.bitwarden = {
    enable = mkEnableOption (mdDoc "bitwarden");
  };

  config = mkIf cfg.enable {
    programs.rbw = {
      enable = true;
      settings = {
        email = "vogesclayton@gmail.com";
        lock_timeout = 1000;
        #base_url = "";
        #identity_url = "";
      };
    };
  };
}
