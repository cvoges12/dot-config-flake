{ config, lib, ... }:

with lib;

let
  cfg = config.localModules.mpv;
in {
  options.localModules.mpv = {
    enable = mkEnableOption (mdDoc "mpv");
  };

  config = mkIf cfg.enable {
    programs = {
      mpv = {
        enable = true;
        config = {
          hwdec = "auto-safe";
          vo = "gpu";
          profile = "gpu-hq";
          force-window = true;
          ytdl-format = "bestvideo+bestaudio";
          #cache-default = 4000000;
        } // attrsets.optionalAttrs (!config.xsession.enable) {
          gpu-context = "wayland";
        };
      };
      yt-dlp = {
        enable = true;
        settings = {
          embed-thumbnail = true;
          embed-subs = true;
          sub-langs = "all";
        };
      };
      aria2.enable = true;
    };
  };
}
