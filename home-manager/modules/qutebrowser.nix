{ config, lib, ... }:

with lib;

let
  cfg = config.localModules.qutebrowser;
in {
  options.localModules.qutebrowser = {
    enable = mkEnableOption (mdDoc "qutebrowser");
  };

  config = mkIf cfg.enable {
    programs.qutebrowser = {
      enable = true;
      keyMappings = {};
      quickmarks = {};
      searchEngines = {};
      settings = {};
    };
  };
}
