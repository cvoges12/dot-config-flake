{ config, lib, ... }:

with lib;

let
  cfg = config.localModules.kitty;
in {
  options.localModules.kitty = {
    enable = mkEnableOption (mdDoc "kitty");
  };

  config = mkIf cfg.enable {
    programs.kitty = {
      enable = true;

      settings = {
        "background_opacity" = "0.6";
        "cursor_shape" = "block";
        "confirm_os_window_close" = "-1";
      } // attrsets.optionalAttrs (!config.xsession.enable) {
        "linux_display_server" = "wayland";
      };

      # fix confirm_os_window_close / shell_integration
      shellIntegration.mode = "no-cursor";
      font = {
        name = "JetBrainsMono Nerd Font Mono";
        size = 10;
      };
      theme = "Dracula";
    };
  };
}
